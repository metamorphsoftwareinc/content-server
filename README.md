## Summary ##
The META tools are built on the idea of *component-based design*. An existing library of components helps a user put together designs quickly using the tools. The bigger the library, the better. However, with a bigger library comes greater difficulty in locating the individual component that you want.

What we need is a a server that makes it possible to query the library and then retrieve a desired component. A UI element on the client side will handle the user interactions, but repository is specific to the back-end server that implements this capability.

## Running ##

The server is run by going into the root of the application directory and typing:  node bin/www

The default port (as setup by express) is 3000.

## Interface ##
This section describes the requirements for server's interface.

### Technology ###
REST APIs.

## REST API Specs ##

### GET classification/tree ###

Retrieves classification tree.

**Parameters**

**name** | **required** | **description** | **example values**
-------------|-----------------|---------------------|---------------------------
*fromRoot* | *optional* | Path of component category to query below. If not set, root will be assumed | 'rewewrewrew3'
*count* | *optional* | Tells how many levels deep, defaults to 2 if not set. | 4

*fromRoot* uses '!' to separate the different path elements.  '!' can be used by itself to specify the root.

**Example response:**

```
#!javascript

{
    classes: [
        {
            id: "connectors_and_adapters",
            label: "Connectors and adapters",
            categoryTotal: 4,
            childCategoriesCount: 1,
            childComponentsCount: 0,
            subClasses: [
                {
                    id: "connectors_and_adapters/connectors",
                    label: "Connectors",
                    categoryTotal: 4,
                    childCategoriesCount: 2,
                    childComponentsCount: 1
                }
            ]
        }
    ]
}
```

### GET components/list ###

Retrieves component overview for a list of components.

**Parameters**

**name** | **required** | **description** | **example values**
-------------|-----------------|---------------------|---------------------------
class | *optional* | Path of component category to query below. If not set, root will be assumed. | 'passive_components'
count | *optional* | Tells how many components to return. Defaults to 50  | 20
cursor | *optional* | If multiple pages of components were returned earlier, results can be retrieved from a specified cursor. If not set defaults to -1. | 40


*class* uses '!' to separate the different path elements.  '!' can be used by itself to specify the root.

**Example response**


```
#!javascript

{
    components: [
        {
            id: "metamorph-7158da08f92d22ca",
            name: "R 01005 150 0.03W 1% Thick Film Samsung RC0402F151CS",
            classificationLabels: [
                "passive_components",
                "resistors",
                "single_components"
            ],
            prominentProperties: [
                {
                    name: "R",
                    value: "150",
                    units: "ohm"
                },
                {
                    name: "Power",
                    value: "0.03W, 1/32W"
                },
                ...
            ],
            otherProperties: [
                {
                    name: "Octopart mpn",
                    value: "RC0402F151CS"
                },
                {
                    name: "Manufacturer",
                    value: "Samsung Electro-Mechanics America, Inc"
                },
                {
                    name: "Cp",
                    value: "0.006e-12",
                    units: "F"
                },
                {
                    name: "Ls",
                    value: "0.13e-9",
                    units: "H"
                },
                ...
            ],
            position: 34
        },
        ...
    ]
}
```


### GET components/overview/:id ###

Returns detailed information on a single component specified with id.

**Parameters**

**name** | **required** | **description** | **example values**
-------------|-----------------|---------------------|---------------------------
id | *required* | Id of component to get. | 'metamorph-6bcbb67a9d8b709c'

**Example response**


```
#!javascript

{
    id: "metamorph-7158da08f92d22ca",
    name: "R 01005 150 0.03W 1% Thick Film Samsung RC0402F151CS",
    classificationLabels: [
        "passive_components",
        "resistors",
        "single_components"
    ],
    prominentProperties: [
        {
            name: "R",
            value: "150",
            units: "ohm"
        },
        {
            name: "Power",
            value: "0.03W, 1/32W"
        },
        ...
    ],
    otherProperties: [
        {
            name: "Octopart mpn",
            value: "RC0402F151CS"
        },
        {
            name: "Manufacturer",
            value: "Samsung Electro-Mechanics America, Inc"
        },
        {
            name: "Cp",
            value: "0.006e-12",
            units: "F"
        },
        {
            name: "Ls",
            value: "0.13e-9",
            units: "H"
        },
        ...
    ],
    position: 34
}
```

### GET components/search


Retrieves components that match the given criteria.  Also supports faceted searches.

**Parameters**

**name** | **required** | **description** | **example values**
-------------|-----------------|---------------------|---------------------------
fromRoot | *optional* | Path of component category to query below. If not set, root will be assumed. | 'passive_components'
searchText | *optional* | find components that contain the provided search text | 'Resistor'
count | *optional* | Tells how many components to return. Defaults to 50  | 20
cursor | *optional* | If multiple pages of components were returned earlier, results can be retrieved from a specified cursor. If not set defaults to -1. | 40


*fromRoot* uses '!' to separate the different path elements.  '!' can be used by itself to specify the root.

Additionally, information to perform a faceted search may, optionally, be passed to the service via a data element named 'query' or 'body'.  A sample of its layout is as follows:


```
#!javascript
{
    Resistance: 1000,
    Manufacturer: 'Panasonic Electronic Components'
}

```

Faceted search data may also include ranges as follows:

**Query String** | **Meaning**
--------------- | ------------------------------------
'>100' | greater than 100
'>=100' | greater than or equal to 100
'<100' | less than 100
'<=100' | less than or equal to 100
'100-500' | 100 to 500


**Example response**


```
#!javascript


{
    total: 317,
    max_score: 1.223,
    component: [
        {
            id: "metamorph-7158da08f92d22ca",
            name: "R 01005 150 0.03W 1% Thick Film Samsung RC0402F151CS",
            classificationLabels: [
                "passive_components",
                "resistors",
                "single_components"
            ],
            prominentProperties: [
                {
                    name: "R",
                    value: "150",
                    units: "ohm"
                },
                {
                    name: "Power",
                    value: "0.03W, 1/32W"
                },
                ...
            ],
            otherProperties: [
                {
                    name: "Octopart mpn",
                    value: "RC0402F151CS"
                },
                {
                    name: "Manufacturer",
                    value: "Samsung Electro-Mechanics America, Inc"
                },
                {
                    name: "Cp",
                    value: "0.006e-12",
                    units: "F"
                },
                {
                    name: "Ls",
                    value: "0.13e-9",
                    units: "H"
                },
                ...
            ],
            position: 34
        },
        ...
    ]
}
```

### GET getcomponent/:type/:id

Packages all the information associated with the specified component into a zip file and provides it to the front-end.
This is accomplished in a two step process, i.e. this service must be called twice (with different parameters).
The first call supplies the file name.  The second call supplies the zip file contents.


**Parameters - Get Name**

**name** | **required** | **description** | **example values**
-------------|-----------------|---------------------|---------------------------
type | *required* | get file name | f
id | *required* | Id of component to get. | 'metamorph-6bcbb67a9d8b709c'

The name of the zip file is returned.


**Parameters - Get Content**

**name** | **required** | **description** | **example values**
-------------|-----------------|---------------------|---------------------------
type | *required* | get file content | c
id | *required* | File name returned from first call | 'resistor.zip'


The zip file contents are returned.

### GET subcircuit/categories

Returns a list of the available sub-circuits.


