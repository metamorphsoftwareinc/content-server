/*globals angular*/

'use strict';

angular.module(
    'mms.componentBrowser.componentLibrary', [])
    .provider('componentLibrary', function ComponentLibraryProvider() {
        var serverUrl;

        this.setServerUrl = function (url) {
            serverUrl = url;
        };

        this.$get = [

            '$http',
            '$q',
            '$log',

            function ($http, $q, $log) {

                var ComponentLibrary,
                    encodeCategoryPath;

                encodeCategoryPath = function(path) {
                    return path.replace(/\//g, '!');
                };

                ComponentLibrary = function () {

                    var classificationTree,
                        subcircuitTree,
                        grandTotal,
                        subcircuitGrandTotal;

                    this.getServerUrl = function() {
                        return serverUrl;
                    };

                    this.getListOfComponents = function(categoryPath, itemCount, cursor ) {

                        var deferred = $q.defer();

                        /*  Approach changed from getting all components in a category to getting all components in
                            the current category and all categories below.
                         */

                        //url = serverUrl + '/components/list/' + encodeCategoryPath(categoryPath) +
                        //    '/' + itemCount + '/' + cursor;
                        //
                        //$http.get(url)
                        //
                        //    .success(function (data) {
                        //        deferred.resolve(data.components);
                        //    })
                        //    .error(function (e) {    // assume attempt to page past available components
                        //        deferred.reject('Could not load list of components', e);
                        //});

                        $http.get(
                            serverUrl + '/components/search' + '/' + encodeCategoryPath(categoryPath) +
                            '/' + '_all' + '/' + itemCount + '/' + cursor)
                            .success(function(data){
                                deferred.resolve(data);
                            })
                            .error(function (e) {
                                deferred.reject('Could not perform search', e);
                            });

                        return deferred.promise;
                    };

                    this.getListOfSubCircuits = function(categoryPath, itemCount, cursor ) {

                        var deferred = $q.defer();

                        $http.get(serverUrl + '/subcircuit/list/' + encodeCategoryPath(categoryPath) +
                            '/' + itemCount + '/' + cursor)

                            .success(function (data) {
                                deferred.resolve(data.subcircuits);
                            })
                            .error(function (e) {    // assume attempt to page past available components
                                deferred.reject('Could not load list of sub-circuits', e);
                        });

                        return deferred.promise;
                    };


                    this.getClassificationTree = function (id) {

                        var deferred = $q.defer(),
                            url;

                        url = serverUrl + '/classification/tree';

                        if (angular.isString(id)) {
                            url += '/' + encodeCategoryPath(id);
                        }

                        $http.get(url)

                            .success(function (data) {

                                classificationTree = data.classes;
                                deferred.resolve(classificationTree);
                                grandTotal = data.grandTotal;

                            })
                            .error(function (data, status, headers, config) {
                                deferred.reject({
                                    msg: 'Could not load component classification tree',
                                    data: data,
                                    status: status,
                                    headers: headers,
                                    config: config
                                });
                            });

                        return deferred.promise;
                    };

                    this.getSubCircuitTree = function (id) {

                        var deferred = $q.defer(),
                            url;

                        url = serverUrl + '/subcircuit/classification/tree';

                        if (angular.isString(id)) {
                            url += '/' + encodeCategoryPath(id);
                        }

                        $http.get(url)

                            .success(function (data) {

                                subcircuitTree = data.classes;
                                deferred.resolve(subcircuitTree);
                                subcircuitGrandTotal = data.grandTotal;

                            })
                            .error(function (data, status, headers, config) {
                                deferred.reject({
                                    msg: 'Could not load sub-circuit classification tree',
                                    data: data,
                                    status: status,
                                    headers: headers,
                                    config: config
                                });
                            });

                        return deferred.promise;
                    };

                    var downloadURL = function (url) {
                        var hiddenIFrameID = 'hiddenDownloader',
                            iframe = document.getElementById(hiddenIFrameID);
                        if (iframe === null) {
                            iframe = document.createElement('iframe');
                            iframe.id = hiddenIFrameID;
                            iframe.style.display = 'none';
                            document.body.appendChild(iframe);
                        }
                        iframe.src = url;
                    };

                    this.downloadItem = function(id) {

                        $log.debug('Download handler');

                        // console.log(serverUrl);

                        window.location = serverUrl + '/getcomponent/download/' + id;
                    };

                    // TODO: cleanup subcircuit stuff

                    this.downloadSubcircuit = function(id) {

                        $log.debug('Download handler');

                        downloadURL(serverUrl + '/subcircuit/getsubcircuit/download/' + id);
                    };

                    this.getGrandTotal = function() {
                        return grandTotal;
                    };

                    this.getSubcircuitGrandTotal = function() {
                        return subcircuitGrandTotal;
                    };

                    this.searchComponents = function(categoryPath, globalSearchText, itemCount, cursor, columnSearchText, sortColumns) {

                        var deferred = $q.defer();

                        globalSearchText = globalSearchText === undefined || globalSearchText === null || globalSearchText === '' ? '_all' : globalSearchText;

                        sortColumns = sortColumns === undefined || sortColumns === null ? [] : sortColumns;

                        var parameters = {};
                        parameters.columnSearchText = columnSearchText;
                        parameters.sortColumns = sortColumns;

                        $http({
                            url: serverUrl + '/components/search' + '/' + encodeCategoryPath(categoryPath) + '/' + globalSearchText + '/' + itemCount + '/' + cursor,
                            method: 'GET',
                            params: parameters
                        })
                            .success(function(data){

                                if (data && angular.isArray(data.component) && data.component.length) {
                                    deferred.resolve(data);
                                } else {
                                    data = {};
                                    data.component = [];  //  no data
                                    deferred.resolve(data);
                                }

                            })
                            .error(function (e) {
                                deferred.reject('Could not perform search', e);
                            });

                        return deferred.promise;

                    };

                    this.searchSubCircuits = function(categoryPath, globalSearchText, itemCount, cursor) {

                        var deferred = $q.defer();

                        globalSearchText = globalSearchText === undefined || globalSearchText === null || globalSearchText === '' ? '_all' : globalSearchText;

                        $http({
                            url: serverUrl + '/subcircuit/search' + '/' + encodeCategoryPath(categoryPath) + '/' + globalSearchText + '/' + itemCount + '/' + cursor,
                            method: 'GET'
                        })
                            .success(function(data){

                                if (data && angular.isArray(data.subcircuit) && data.subcircuit.length) {
                                    deferred.resolve(data);
                                } else {
                                    data = {};
                                    data.subcircuit = [];  //  no data
                                    deferred.resolve(data);
                                }

                            })
                            .error(function (e) {
                                deferred.reject('Could not perform search', e);
                            });

                        return deferred.promise;

                    };

                    this.getDetails = function (id) {

                        var self = this,
                            path = serverUrl + '/components/overview/' + id,
                            deferred = $q.defer();


                        $http.get(path)

                            .then(function (json) {

                                var documentation = {
                                    id: id,
                                    description: json.data.description,
                                    connectors: json.data.connectors
                                };

                                if (json.data.iconFileName) {
                                    documentation.icon = self.getServerUrl() + '/component_files/' + json.data.iconFileName;
                                }


                                deferred.resolve(documentation);

                            });

                        return deferred.promise;

                    };

                };

                return new ComponentLibrary();

            }
        ];
    });
