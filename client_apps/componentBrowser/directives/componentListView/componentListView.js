/**
 * Created by Blake McBride on 2/16/15.
 */

/*global angular*/

'use strict';

require('../../services/componentLibrary.js');
require('../downloadButton/downloadButton.js');
require('../../../componentBrowser/directives/showMoreButton/showMoreButton.js');
require('../../../componentBrowser/directives/showLessButton/showLessButton.js');
require('../infoButton/infoButton.js');
require('../componentDetails/componentDetails.jsx');

var listViewBase = require('../../../common/listViewBase.js');

angular.module('mms.componentBrowser.componentListView', [
    'isis.ui.itemList',
    'mms.componentBrowser.componentLibrary',
    'mms.componentBrowser.downloadButton',
    'mms.componentBrowser.showMoreButton',
    'mms.componentBrowser.showLessButton',
    'mms.componentBrowser.infoButton',
    'mms.componentDetails.react'
])
    .controller('ComponentListViewItemController', function () {
        // console.log($scope);
        //debugger;
    })

    .directive('componentListView', function () {

        function ComponentDetailsController($scope) {

            var commonList = listViewBase.call(this, $scope, this.contentLibraryService);

            var config = commonList.config,
                self,
                findOctopart,
                renderList;

            self = this;

            this.listData = {
                items: []
            };

            if (typeof this.onItemDragStart === 'function' &&
                typeof this.onItemDragEnd === 'function') {

                config.onItemDragStart = function(e, item) {
                    self.onItemDragStart(e, item);
                };

                config.onItemDragEnd = function(e, item) {
                    self.onItemDragEnd(e, item);
                };

            }

            this.config = config;

            findOctopart = function(comp) {
                var oprops;
                var i, prop;
                if (comp.componentProperties === undefined) {
                    oprops = comp.otherProperties;
                    if (Array.isArray(oprops)) {
                        for (i in oprops) {
                            prop = oprops[i];
                            if (prop.name !== undefined && prop.name.toLowerCase().indexOf('octopart') === 0) {
                                return prop.value;
                            }
                        }
                    }
                } else {
                    oprops = comp.componentProperties;
                    if (Array.isArray(oprops)) {
                        for (i in oprops) {
                            prop = oprops[i];
                            if (prop.name !== undefined && prop.name.toLowerCase().indexOf('octopart') === 0) {
                                return prop.stringValue;
                            }
                        }
                    }
                }
                return undefined;
            };

            renderList = function () {

                var comps = [];

                for (var i in self.components) {
                    var comp = self.components[i],
                        item = commonList.itemGenerator(comp, 'component', '/componentBrowser/templates/');

                    item.octopart = findOctopart(comp);
                    item.expandDetails = self.expandDetails;

                    comps.push(item);
                }
                self.listData.items = comps;

            };

            $scope.$watchCollection(function () {
                return self.components;
            }, function () {
                renderList();
            });

            renderList();

            this.expandDetails = function(item) {

                self.contentLibraryService.getDetails(item.id, item.details)
                    .then(function(documentation) {

                        item.details.documentation = documentation;
                        item.expandedDetails = true;

                    });

            };

        }


        return {
            restrict: 'E',
            replace: true,
            controller: ComponentDetailsController,
            controllerAs: 'ctrl',
            bindToController: true,
            templateUrl: '/componentBrowser/templates/componentListView.html',
            scope: {
                components: '=',
                onItemDragStart: '=',
                onItemDragEnd: '=',
                noDownload: '=',
                contentLibraryService: '='
            }
        };
    });
