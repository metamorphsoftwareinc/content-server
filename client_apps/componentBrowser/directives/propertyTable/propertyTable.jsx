'use strict';

class PropertyTable extends React.Component {

    render() {

        this.items = this.props.properties.map( (item, index) => {

            return <PropertyTableItem property={item} ref={index} key={index}/>;
        });

        return <div className="property-table">{this.items}</div>;
    }

    componentDidMount() {

        let nameWidths = [],
            valueWidths = [];


        for (let i in this.refs) {

            // let itemEl = React.findDOMNode(this.refs[i]);
            //
            // itemEls.push(itemEl);

            let item = this.refs[i];

            nameWidths.push(item.getNameWidth());
            valueWidths.push(item.getValueWidth());

        }

        let widestNameWidth = Math.max.apply(null, nameWidths);
        let widestValueWidth = Math.max.apply(null, valueWidths);

        //let itemWidth = widestNameWidth + widestValueWidth;

        for (let i in this.refs) {

            let item = this.refs[i];

            item.setNameWidth(widestNameWidth);
            item.setValueWidth(widestValueWidth);

        }

    }

}


class PropertyTableItem extends React.Component {

    render() {

        var nameEl = <div className="property-name" title={this.props.property.name} ref="name">
                        {this.props.property.name}
                    </div>;

        var valueEl = <div className="property-value" title={this.props.property.value} ref="value">
                        {this.props.property.value}
                    </div>;

        return <div className="property-table-item">{nameEl}{valueEl}</div>;

    }

    getNameWidth() {

        return Math.ceil(
            parseFloat(
                getComputedStyle(React.findDOMNode(this.refs.name)).width
            )
        );
    }

    getValueWidth() {

        return Math.ceil(
            parseFloat(
                getComputedStyle(React.findDOMNode(this.refs.value)).width
            )
        );
    }

    setNameWidth(w) {
        React.findDOMNode(this.refs.name).style.width = w + 'px';
    }

    setValueWidth(w) {
        React.findDOMNode(this.refs.value).style.width = w + 'px';
    }

    setWidth(w) {
        React.findDOMNode(this).style.width = w + 'px';
    }
}

module.exports = PropertyTable;
