/*global angular,numeral*/

'use strict';

angular.module('mms.componentBrowser.countDisplay', [])

    .directive('countDisplay', function () {

        function CountDisplayController($scope) {
//            this.numeral = numeral;
            $scope.numeral = numeral;

            this.getItemsPerPage = function () {
                return self.itemsPerPage;
            };

            this.availableItemsPerPage = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50];

        }

        return {
            restrict: 'E',
            controller: CountDisplayController,
            controllerAs: 'ctrl',
            bindToController: true,
            scope: {
                fromNumber: '=',
                toNumber: '=',
                totalCount: '=',
                itemsPerPage: '=',
                setItemsPerPage: '='
            },
            replace: true,
            templateUrl: '/componentBrowser/templates/countDisplay.html'
        };
    });

