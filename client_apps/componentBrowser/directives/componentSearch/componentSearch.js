/**
 * Created by Blake McBride on 2/23/15.
 */

/*global angular, alert*/

'use strict';

angular.module('mms.componentBrowser.componentSearch', [
])

    .directive('componentSearch', function () {

        function ComponentSearchController() {

            var self;

            self = this;


            this.keydownInSearchField = function($event) {

                if ($event.keyCode === 13) {
                    self.doSearch();
                }

            };

        }


        return {
            restrict: 'E',
            replace: true,
            controller: ComponentSearchController,
            controllerAs: 'ctrl',
            bindToController: true,
            templateUrl: '/componentBrowser/templates/componentSearch.html',
            scope: {
                searchText: '=',
                doSearch: '='
            }
        };
    });
