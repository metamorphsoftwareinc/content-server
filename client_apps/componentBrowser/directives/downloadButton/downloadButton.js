
/*global angular*/

'use strict';
angular.module('mms.componentBrowser.downloadButton', [])

    .directive('downloadButton', function () {

        return {
            restrict: 'E',
            replace: true,
            controllerAs: 'ctrl',
            bindToController: true,
            templateUrl: '/componentBrowser/templates/downloadButton.html'
        };
    });

