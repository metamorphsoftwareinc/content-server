'use strict';

var PropertyTable = require('../propertyTable/propertyTable.jsx');
var ConnectorsDescription = require('../../../subcircuitBrowser/directives/subcircuitDetails/ConnectorsDescription.jsx');

angular.module('mms.componentDetails.react', [])
.directive('componentDetails',
    function() {

        function ComponentDetailsController() {

        }

        return {
            restrict: 'E',
            controller: ComponentDetailsController,
            controllerAs: 'ctrl',
            bindToController: true,
            replace: true,
            transclude: false,
            template: '<div class="component-details"></div>',
            scope: {
                properties: '=',
                details: '='
            },
            require: ['componentDetails'],
            link: function(scope, element, attr, controllers) {

                var ctrl = controllers[0];

                function cleanup() {
                  React.unmountComponentAtNode(element[0]);
                }

                function render() {
                    React.render(<ComponentDetailsGrid details={ctrl.details} properties={ctrl.properties}/>, element[0]);
                }

                scope.$watch(function() {
                    if (ctrl.details) {
                        return ctrl.details;
                    }
                }, function(newO, oldO){

                    if ((oldO !== newO || oldO != null) && newO != null) {

                        cleanup();
                        render();
                    }
                });

                scope.$on('$destroy', cleanup);

            }
        };
    }
);

class ComponentDetailsGrid extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        var className = 'component-details-grid',
            icon;

        if (this.props.details.icon) {

            icon = <div className="component-icon-container">
                <img className="component-icon" src={encodeURI(this.props.details.icon)}/>
            </div>;


        }

        return (
            <div className={className}>
                <PropertyTable properties={this.props.properties}/>
                {icon}
                <ConnectorsDescription connectors={this.props.details.connectors}/>
            </div>
        );
    }
}
