/*global angular*/

'use strict';
angular.module('mms.componentBrowser.showLessButton', [])

    .directive('showLessButton', function () {

        return {
            restrict: 'E',
            replace: true,
            controllerAs: 'ctrl',
            bindToController: true,
            templateUrl: '/componentBrowser/templates/showLessButton.html'
        };
    });
