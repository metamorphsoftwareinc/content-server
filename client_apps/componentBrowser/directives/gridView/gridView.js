/**
 * Created by Blake McBride on 2/26/15.
 */

/*global angular*/

'use strict';

require('../../services/componentLibrary.js');
require('../downloadButton/downloadButton.js');
require('../infoButton/infoButton.js');

angular.module('mms.componentBrowser.gridView', [
    'ui.grid',
    'ui.grid.resizeColumns',
    'mms.componentBrowser.componentLibrary',
    'mms.componentBrowser.downloadButton',
    'mms.componentBrowser.infoButton'
])
    .directive('gridRow', function() {
        return {
            template: '<div class="grid-row"><ng-transclude></ng-transclude></div>',
            transclude: true,
            replace: true,
            link: function(scope, element) {

                    var onDragStart,
                        onDragEnd,
                        itemId = scope.row.entity.id;

                    //console.log(scope.row);

                    onDragStart = function(e) {

                        e.dataTransfer.effectAllowed = 'move';
                        e.dataTransfer.setData('text', itemId);

                        element.addClass('dragged');

                        if (typeof scope.grid.appScope.onItemDragStart === 'function') {
                            scope.grid.appScope.onItemDragStart(e, scope.row.entity);
                        }

                    };

                    onDragEnd = function(e) {

                        element.removeClass('dragged');

                        if (typeof scope.grid.appScope.onItemDragEnd === 'function') {
                            scope.grid.appScope.onItemDragEnd(e, scope.row.entity);
                        }

                    };

                    if (typeof scope.grid.appScope.onItemDragEnd === 'function' && typeof scope.grid.appScope.onItemDragStart === 'function') {

                        element[0].classList.add('draggable');
                        element[0].setAttribute('draggable', 'true');

                        element[0].addEventListener('dragstart', onDragStart);
                        element[0].addEventListener('dragend', onDragEnd);

                    }

                    scope.$on('$destroy', function() {

                        element[0].removeEventListener('dragstart', onDragStart);
                        element[0].removeEventListener('dragend', onDragEnd);

                    });

            }
        };
    })

    .directive('gridView', function () {

        function ComponentGridController($scope, $log, componentLibrary) {

            var self = this;

            var addProperties = function (e, props, oprops, columnDefs) {
                //  add non-prominent properties
                if (oprops !== undefined && oprops !== null) {
                    if (props === undefined || props === null) {
                        props = oprops;
                    } else {
                        props = props.concat(oprops);
                    }
                }
                for (var i in props) {
                    var p = props[i];
                    if (p.name !== 'ComponentName') {
                        var val = '';
                        for (var ii in p) {
                            if (ii !== 'name' && ii !== 'id') {
                                var v = p[ii];
                                if (val !== '') {
                                    val += ' ';
                                }
                                val += v;
                            }
                        }
                        e[p.name] = val;
                        columnDefs[p.name] = null;  // create a placeholder for a unique name
                    }
                }
                return columnDefs;
            };

            var addSearchProperties = function (e, props, columnDefs) {
                var val;
                var build = function(x) {
                    if (x !== undefined && x != null && x !== '') {
                        if (val !== '') {
                            val += ' ';
                        }
                        val += x;
                    }
                };
                for (var i in props) {
                    var p = props[i];
                    val = '';
                    build(p.stringValue);
                    build(p.units);
                    if (p.name !== 'ComponentName') {
                        e[p.name] = val;
                        columnDefs[p.name] = null;  // create a placeholder for a unique name
                    }
                }
                return columnDefs;
            };


            var findOctopart = function(comp) {
                var oprops;
                var i, prop;
                if (comp.componentProperties === undefined) {
                    oprops = comp.otherProperties;
                    if (Array.isArray(oprops)) {
                        for (i in oprops) {
                            prop = oprops[i];
                            if (prop.name !== undefined && prop.name.toLowerCase().indexOf('octopart') === 0) {
                                return prop.value;
                            }
                        }
                    }
                } else {
                    oprops = comp.componentProperties;
                    if (Array.isArray(oprops)) {
                        for (i in oprops) {
                            prop = oprops[i];
                            if (prop.name !== undefined && prop.name.toLowerCase().indexOf('octopart') === 0) {
                                return prop.stringValue;
                            }
                        }
                    }
                }
                return undefined;
            };

            var cellTemplate,
                cellWidth;

            if (!this.noDownload) {

                cellTemplate = '<div class="text-center"><download-button ng-click="grid.appScope.clickHandler(row)"></download-button>' +
                    '<info-button ng-if="row.entity.octopart!==undefined" label="\'View on Octopart\'" ng-click="grid.appScope.infoHandler(row)"></info-button></div>';
                cellWidth = 70;

            } else {

                cellTemplate = '<info-button ng-if="row.entity.octopart!==undefined" label="\'View on Octopart\'" ng-click="grid.appScope.infoHandler(row)"></info-button></div>';
                cellWidth = 30;

            }

            var renderList = function () {
                var grid = [];
                var columnDefs = {name: null};
                var fnames = [{
                    name: ' ',
                    cellTemplate: cellTemplate,
                    width: cellWidth,
                    enableSorting: false,
                    enableColumnResizing: false,
                    enableFiltering: false,
                    cellClass: 'actions'
                }];
                var startsWith = function (s1, s2) {
                    return s1.indexOf(s2) === 0;
                };
                var findColumnIndex = function (fn, name, exact) {
                    var i, fldname;
                    for (i = 0; i < fn.length; i++) {
                        if (fn[i] !== undefined) {
                            fldname = fn[i].field.toLowerCase();
                            if (fldname === name || !exact && startsWith(fldname, name)) {
                                return i;
                            }
                        }
                    }
                    return -1;
                };
                var addColumn = function (colArray, fn, exact, name) {
                    var i = findColumnIndex(fn, name, exact);
                    if (i !== -1) {
                        colArray.push(fn[i]);
                        delete fn[i];
                    }
                };
                var sortColumns = function (fn) {
                    var res = [], i;

                    res.push(fn.shift());

                    addColumn(res, fn, false, 'name');
                    addColumn(res, fn, false, 'octo');
                    addColumn(res, fn, false, 'resistance');
                    addColumn(res, fn, false, 'inductance');
                    addColumn(res, fn, false, 'capac');
                    addColumn(res, fn, true, 'c');
                    addColumn(res, fn, true, 'r');

                    // add the remaining columns
                    for (i = 0; i < fn.length; i++) {
                        if (fn[i] !== undefined) {
                            res.push(fn[i]);
                        }
                    }

                    return res;
                };
                for (var ii in self.components) {
                    var c = self.components[ii];
                    var e = {};
                    e.name = c.name.replace(/_/g, ' ');
                    e.id = c.id;
                    e.octopart = findOctopart(c);
                    if (c.componentProperties === undefined) {
                        columnDefs = addProperties(e, c.prominentProperties, c.otherProperties, columnDefs);
                    } else {
                        columnDefs = addSearchProperties(e, c.componentProperties, columnDefs);
                    }
                    grid.push(e);
                }
                if (!self.lockGridColumns) {
                    self.columnSearchText = {};
                }
                for (var n in columnDefs) {
                    var colInfo;
                    colInfo = {};
                    colInfo.field = n;


                    if (self.columnSearchText[n] !== undefined) {
                        colInfo.filter = { term: self.columnSearchText[n]};
                    }
                    //if (self.columnSortInfo[n] !== undefined) {
                    //    colInfo.sort = {direction: self.columnSortInfo[n]};
                    //}
                    fnames.push(colInfo);


                }
                if (!self.lockGridColumns) {
                    self.gridOptions.columnDefs = sortColumns(fnames);
                }
                self.gridOptions.data = grid;
                // TODO when the Angular team updates ui-grid
//                 self.gridOptions.appScopeProvider = self;

            };

            var columnType = function (rows, colName) {
                var i, val;
                for (i in rows) {
                    val = rows[i].entity[colName];
                    if (val.length > 0 && isNaN(parseInt(val.substr(0, 1)))) {
                        return 'string';
                    }
                }
                return 'numeric';
            };

            self.gridOptions = {
                enableFiltering: true,
                useExternalFiltering: true,
                useExternalSorting: true,
                rowTemplate: '<grid-row>' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader }"  ui-grid-cell></div>' +
                    '</grid-row>',
                columnDefs: [],
                onRegisterApi: function( gridApi ) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function(grid, sortCols) {
                        var i, col, dat;
                        self.columnSortInfo = [];
                        for (i in sortCols) {
                            col = sortCols[i];
                            dat = {};
                            dat.field = col.field;
                            dat.sort = col.sort.direction;
                            dat.type = columnType(this.grid.rows, col.field);
                            self.columnSortInfo.push(dat);
                        }
                        self.lockGridColumns = self.columnSortInfo.length > 0 || Object.keys(self.columnSearchText).length > 0;
                        componentLibrary.searchComponents(self.selectedCategory.path, self.searchText, self.pagingParameters.itemsPerPage, 0, self.columnSearchText, self.columnSortInfo)
                            .then(function (results) {
                                self.facetedSearch = self.columnSearchText;
                                self.setFacetedSearch(self.columnSearchText);
                                self.newData(results);
                            })
                            .catch(function(e) {
                                $log.warn('No results:', e);
                            });

                    });
                    $scope.gridApi.core.on.filterChanged( $scope, function() {
                        var grid = this.grid;
                        var filt, col;
                        var n;

                        self.columnSearchText = {};
                        for (n in grid.columns) {
                            col = grid.columns[n];
                            filt = col.filters;
                            if (filt.length > 0 && filt[0].term !== undefined && filt[0].term !== null && filt[0].term.length > 0) {
                                self.columnSearchText[col.name] = filt[0].term;
                            }
                        }
                        self.lockGridColumns = self.columnSortInfo.length > 0 || Object.keys(self.columnSearchText).length > 0;
                        // cols has the search string
                        var st = self.searchText;
                        componentLibrary.searchComponents(self.selectedCategory.path, st, self.pagingParameters.itemsPerPage, 0, self.columnSearchText, self.columnSortInfo)
                            .then(function (results) {
                                self.facetedSearch = self.columnSearchText;
                                self.setFacetedSearch(self.columnSearchText);
                                self.newData(results);
                            })
                            .catch(function(e) {
                                $log.warn('No results:', e);
                            });
                    });
                }
            };

            $scope.onItemDragStart = this.onItemDragStart;
            $scope.onItemDragEnd = this.onItemDragEnd;

            $scope.clickHandler = function (row) {
                componentLibrary.downloadItem(row.entity.id);
            };

            $scope.infoHandler = function (row) {
                if (row.entity.octopart !== undefined) {
                    var url = 'http://octopart.com/search?q=' + row.entity.octopart + '&view=list';
                    var win = window.open(url, '_blank');
                    win.focus();
                }
            };

            $scope.$watch(function () {
                return self.components;
            }, function () {
                renderList();
            });

//            renderList();
        }

        return {
            restrict: 'E',
            replace: true,
            controller: ComponentGridController,
            controllerAs: 'ctrl',
            bindToController: true,
            templateUrl: '/componentBrowser/templates/gridView.html',
            scope: {
                components: '=',
                selectedCategory: '=',
                searchText: '=',
                columnSearchText: '=',
                columnSortInfo: '=',
                pagingParameters: '=',
                newData: '=',
                facetedSearch: '=',
                setFacetedSearch: '=',
                lockGridColumns: '=',
                onItemDragStart: '=',
                onItemDragEnd: '=',
                noDownload: '='
            }
        };
    });
