/**
 * Created by Blake McBride on 3/27/15.
 */

/*global angular*/

'use strict';
angular.module('mms.componentBrowser.infoButton', [])

    .directive('infoButton', function () {

        return {
            scope: {
                label: '='
            },
            restrict: 'E',
            replace: true,
            controllerAs: 'ctrl',
            bindToController: true,
            templateUrl: '/componentBrowser/templates/infoButton.html'
        };
    });
