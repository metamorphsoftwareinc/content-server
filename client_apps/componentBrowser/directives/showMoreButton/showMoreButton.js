/*global angular*/

'use strict';
angular.module('mms.componentBrowser.showMoreButton', [])

    .directive('showMoreButton', function () {

        return {
            restrict: 'E',
            replace: true,
            controllerAs: 'ctrl',
            bindToController: true,
            templateUrl: '/componentBrowser/templates/showMoreButton.html'
        };
    });
