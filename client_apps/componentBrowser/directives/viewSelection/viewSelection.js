/**
 * Created by Blake McBride on 2/26/15.
 */

/*global angular*/

'use strict';

angular.module('mms.componentBrowser.viewSelection', [])

.directive('viewSelection', function() {

    function ViewSelectionController() {

        var self = this;

        this.selectView = function(view) {

            self.selectedView = view;

            if (typeof self.onViewSelection === 'function') {
                self.onViewSelection({
                    view: view
                });
            }

        };

    }

    return {
        restrict: 'E',
        scope: {
            selectedView: '=',
            onViewSelection: '&'
        },
        replace: true,
        controller: ViewSelectionController,
        bindToController: true,
        controllerAs: 'ctrl',
        templateUrl: '/componentBrowser/templates/viewSelection.html'
    };
});
