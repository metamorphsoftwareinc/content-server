/**
 * Created by Blake McBride on 2/23/15.
 */

/*global angular, alert*/

'use strict';

require('../componentListView/componentListView.js');
require('../gridView/gridView.js');
require('../countDisplay/countDisplay.js');
require('../viewSelection/viewSelection.js');
require('../paging/paging.js');
require('../../services/componentLibrary.js');

angular.module('mms.componentBrowser.componentListing', [
    'mms.componentBrowser.componentListView',
    'mms.componentBrowser.gridView',
    'mms.componentBrowser.viewSelection',
    'mms.componentBrowser.countDisplay',
    'mms.componentBrowser.paging',
    'mms.componentBrowser.componentLibrary'

])

.directive('componentListing', function (componentLibrary) {

    function ComponentListingController() {

        var self = this;

        this.contentLibraryService = componentLibrary;

        this.onViewSelection = function (view) {

            if (typeof self.onListingViewSelection === 'function') {
                self.onListingViewSelection({
                    view: view
                });
            }


        };

    }


    return {
        restrict: 'E',
        controller: ComponentListingController,
        controllerAs: 'ctrl',
        bindToController: true,
        scope: {
            componentsToList: '=',
            pagingParameters: '=',
            getNextPage: '=',
            getPrevPage: '=',
            selectedCategory: '=',
            searchText: '=',
            columnSearchText: '=',
            columnSortInfo: '=',
            newData: '=',
            facetedSearch: '=',
            setFacetedSearch: '=',
            lockGridColumns: '=',
            selectedView: '=',
            onListingViewSelection: '&',
            onItemDragStart: '=',
            onItemDragEnd: '=',
            noDownload: '=',
            setItemsPerPage: '=',
            resultsForSearchText: '=',
            appBooting: '='
        },
        replace: true,
        templateUrl: '/componentBrowser/templates/componentListing.html'
    };
});
