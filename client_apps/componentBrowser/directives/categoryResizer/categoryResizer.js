/*global angular*/

'use strict';

angular.module('mms.contentBrowser.categoryResizer', [
        'ngCookies'
    ])
    .directive('categoryResizer',
        function($cookies, $timeout) {

            function ResizerController() {

                this._startingWidth = 300;
                this._minWidth = 200;
                this._maxWidth = 500;

                this._panelDragging = false;
                this._potentialPanelDragStart = null;
                this._widthBeforePanelDragging = null;

                this._widthAllowance = null;


            }

            ResizerController.prototype.getWidth = function() {
                return this._width;
            };

            ResizerController.prototype.setWidth = function(width) {

                if (!isNaN(width) && width >= this._minWidth && width <= this._maxWidth) {

                    this._width = width;

                    this._updateResizerPosition();

                    if (this._categoryPanelEl) {
                        this._categoryPanelEl.style.width = Math.floor(this._width) + 'px';
                    }

                    if (this._detailPanelEl) {
                        this._detailPanelEl.style.width = Math.floor(this._widthAllowance - this._width) + 'px';
                    }

                    $timeout(function() {
                        $cookies.categoryWidth = width;
                    });

                } else {
                    this._stopPanelDragging();
                }

            };

            ResizerController.prototype._updateResizerPosition = function() {

                this._resizerEl.style.left = this._width + 'px';

            };

            ResizerController.prototype._init = function() {

                this._widthAllowance =
                    parseInt(getComputedStyle(this._resizerEl.parentElement).width.slice(0, -2), 10);
                this.setWidth(this._startingWidth);
            };

            ResizerController.prototype.panelMouseDown = function($event) {

                if (!this._panelDragging) {
                    this._potentialPanelDragStart = $event.clientX;
                    $event.preventDefault();
                }

            };

            ResizerController.prototype._startPanelDragging = function() {

                this._panelDragging = true;
                this._widthBeforePanelDragging = this.getWidth();
                this._widthAllowance =
                    parseInt(getComputedStyle(this._resizerEl.parentElement).width.slice(0, -2), 10);

            };

            ResizerController.prototype._stopPanelDragging = function() {
                this._panelDragging = false;
            };

            ResizerController.prototype._resizerMouseUp = function() {

                this._potentialPanelDragStart = null;

                if (this._panelDragging) {
                    this._stopPanelDragging();
                }

            };

            ResizerController.prototype._resizerMouseMove = function($event) {

                var newwidth,
                    offset;

                if (!this._panelDragging && this._potentialPanelDragStart != null) {
                    this._startPanelDragging();
                }

                if (this._panelDragging) {

                    newwidth = this._widthBeforePanelDragging + ($event.clientX - this._potentialPanelDragStart);

                    offset = Math.min(newwidth, this._widthAllowance) - this._width;

                    this.setWidth(

                        Math.min(newwidth, this._widthAllowance),
                        offset

                    );

                }

            };

            ResizerController.prototype._correctResizerHeight = function() {
                this._resizerEl.style.height = this._categoryPanelEl.style.height;
            };

            return {
                restrict: 'E',
                scope: true,
                controller: ResizerController,
                controllerAs: 'ctrl',
                bindToController: true,
                replace: true,
                transclude: true,
                templateUrl: '/componentBrowser/templates/categoryResizer.html',
                require: ['categoryResizer'],
                link: function(scope, element, attributes, controllers) {

                    var ctrl = controllers[0],
                        boundResizerMouseUp = ctrl._resizerMouseUp.bind(ctrl),
                        boundResizerMouseMove = ctrl._resizerMouseMove.bind(ctrl),
                        boundParentWindowResize = ctrl._init.bind(ctrl),
                        parentElement;


                    ctrl._resizerEl = element[0];

                    if($cookies.categoryWidth && !isNaN($cookies.categoryWidth)) {
                        ctrl._startingWidth = parseInt($cookies.categoryWidth, 10);

                    } else if (attributes.startingWidth && !isNaN(attributes.startingWidth)) {
                        ctrl._startingWidth = attributes.startingWidth;
                    }

                    if (attributes.minWidth && !isNaN(attributes.minWidth)) {
                        ctrl._minWidth = attributes.minWidth;
                    }

                    if (attributes.maxWidth && !isNaN(attributes.maxWidth)) {
                        ctrl._maxWidth = attributes.maxWidth;
                    }

                    parentElement = ctrl._resizerEl.parentElement;

                    ctrl._categoryPanelEl = parentElement.querySelector('.left-panel');
                    ctrl._detailPanelEl = parentElement.querySelector('.main-container-panel');

                    ctrl._init();

                    ctrl._padding = parseInt(getComputedStyle(ctrl._resizerEl).width.slice(0, -2), 10);

                    document.addEventListener('mouseup', boundResizerMouseUp);
                    document.addEventListener('mousemove', boundResizerMouseMove);
                    window.addEventListener('resize', boundParentWindowResize);

                    if (ctrl._categoryPanelEl) {
                        ctrl._correctResizerHeight();
                    }

                    scope.$on('$destroy', function() {

                        if (ctrl._resizerEl) {
                            document.removeEventListener('mouseup', boundResizerMouseUp);
                            document.removeEventListener('mousemove', boundResizerMouseMove);
                            window.removeEventListener('resize', boundParentWindowResize);
                        }

                    });

                }
            };
        }
    );
