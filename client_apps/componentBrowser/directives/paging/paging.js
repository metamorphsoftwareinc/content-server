/**
 * Created by Blake McBride on 2/24/15.
 */

/*global angular*/

'use strict';

angular.module('mms.componentBrowser.paging', [])

    .directive('paging', function () {

        function PagingController() {

            var self;

            self = this;

            this.nextPage = function () {
                if (angular.isFunction(self.onNextPage)) {
                    self.onNextPage();
                }
            };


            this.prevPage = function () {
                if (angular.isFunction(self.onPrevPage)) {
                    self.onPrevPage();
                }
            };

            this.canNextPage = function () {

                var result = false;

                if (self.config && self.config.toNumber < self.config.totalCount) {
                    result = true;
                }

                return result;

            };


            this.canPrevPage = function () {

                var result = false;

                if (self.config && self.config.fromNumber - self.config.itemsPerPage > 0) {
                    result = true;
                }

                return result;

            };
        }

        return {

        restrict: 'E',
            replace: false,
            controller: PagingController,
            controllerAs: 'ctrl',
            bindToController: true,
            templateUrl: '/componentBrowser/templates/paging.html',
            scope: {
                config: '=',
                onPrevPage: '=',
                onNextPage: '='
            }
        };
    });
