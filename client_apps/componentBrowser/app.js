/*globals angular*/
/**
 * Created by blake on 2/9/15.
 */

'use strict';

require('./directives/componentBrowser/componentBrowser');
require('./services/componentLibrary.js');
require('./appConfig');

angular.module('mms.componentBrowserApp', [
    'mms.componentBrowser',
    'mms.componentBrowser.config',
    'mms.componentBrowser.componentLibrary',
    'ngMaterial'
])
    .config(function (componentLibraryProvider, componentServerUrl) {
        if (componentServerUrl.indexOf('http') !== 0) {
            componentServerUrl = window.location.origin + componentServerUrl;
        }
        componentLibraryProvider.setServerUrl(componentServerUrl);
    })

    .controller('AppController', function ($scope) {

        $scope.itemDragStart = function(e, item) {
            console.log('Dragging', e, item);
        };

        $scope.itemDragEnd = function(e, item) {
            console.log('Finish dragging', e, item);
        };

    });

