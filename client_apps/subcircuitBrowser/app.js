/*globals angular*/

'use strict';

require('./directives/subcircuitBrowser/subcircuitBrowser');
require('./services/subcircuitLibrary.js');
require('../componentBrowser/services/componentLibrary.js');
require('./appConfig');

angular.module('mms.subcircuitBrowserApp', [
    'mms.subcircuitBrowser',
    'mms.subcircuitBrowser.config',
    'mms.subcircuitBrowser.subcircuitLibrary',
    'mms.componentBrowser.componentLibrary',
    'ngMaterial'
])
    .config(function (subcircuitLibraryProvider, subcircuitServerUrl, componentLibraryProvider, componentServerUrl) {
        if (subcircuitServerUrl.indexOf('http') !== 0) {
            subcircuitServerUrl = window.location.origin + subcircuitServerUrl;
        }
        subcircuitLibraryProvider.setServerUrl(subcircuitServerUrl);

        if (componentServerUrl.indexOf('http') !== 0) {
            componentServerUrl = window.location.origin + componentServerUrl;
        }
        componentLibraryProvider.setServerUrl(componentServerUrl);
    })

    .controller('AppController', function ($scope) {

        $scope.itemDragStart = function(e, item) {
            console.log('Dragging', e, item);
        };

        $scope.itemDragEnd = function(e, item) {
            console.log('Finish dragging', e, item);
        };

    });

