'use strict';

var ConnectorsDescription = require('./ConnectorsDescription.jsx');

angular.module('mms.subcircuitDetails.react', [])
    .directive('subcircuitDetails',
        function() {

            function SubcircuitDetailsController() {

            }

            return {
                restrict: 'E',
                controller: SubcircuitDetailsController,
                controllerAs: 'ctrl',
                bindToController: true,
                replace: true,
                transclude: false,
                template: '<div class="subcircuit-details"></div>',
                scope: {
                    details: '='
                },
                require: ['subcircuitDetails'],
                link: function(scope, element, attr, controllers) {

                    var ctrl = controllers[0];

                    function cleanup() {
                      React.unmountComponentAtNode(element[0]);
                    }

                    function render() {
                        React.render(<SubcircuitDetailsGrid details={ctrl.details}/>, element[0]);
                    }

                    scope.$watch(function() {
                        if (ctrl.details) {
                            return ctrl.details;
                        }
                    }, function(newO, oldO){

                        if ((oldO !== newO || oldO != null) && newO != null) {

                            cleanup();
                            render();
                        }
                    });

                    scope.$on('$destroy', cleanup);

                }
            };
        }
    );

var SubcircuitDetailsGrid = React.createClass({

	render: function() {

        var className = 'subcircuit-details-grid',
            subcircuitVisuals;

        if (!this.props.details.visuals) {
            className += ' no-visuals';
        } else {
            subcircuitVisuals = <SubcircuitVisuals images={this.props.details.visuals}></SubcircuitVisuals>;
        }

        return (
            <div className={className}>
                <SubcircuitDescription description={this.props.details.description} icon={this.props.details.icon}/>
                {subcircuitVisuals}
                <ConnectorsDescription connectors={this.props.details.connectors}/>
            </div>
        );
	}
});


var SubcircuitDescription = React.createClass({

	render: function() {

        var className = 'subcircuit-description',
            icon;

        if (!this.props.icon) {
            className += ' no-icon';
        } else {
            icon = <img src={encodeURI(this.props.icon)} className="subcircuit-icon"/>;
        }

        return (
            <div className={className}>
                <div className="subcircuit-description-text">
                    {this.props.description}
                </div>
                <div className="subcircuit-icon-container">
                    {icon}
                </div>
            </div>
        );
	}
});


var SubcircuitVisuals = React.createClass({

	render: function() {

        var activeVisual = 0;

        var images = this.props.images.map(function(imageUrl, index) {

            var className = 'subcircuit-visual';

            if (index === activeVisual) {
                className += ' active';
            }

            return <img src={encodeURI(imageUrl)} className={className}/>;
        });

        return (
            <div className="subcircuit-visuals">
                {images}
            </div>
        );
	}
});
