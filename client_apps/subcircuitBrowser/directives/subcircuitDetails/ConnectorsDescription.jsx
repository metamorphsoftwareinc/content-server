'use strict';

var ConnectorsDescription = React.createClass({

	render: function() {

        var title,
            connectors;

        if (Array.isArray(this.props.connectors)) {

            this.props.connectors.sort(function(a, b) {
                if (a.name < b.name) {
                    return -1;
                } else if (a.name === b.name){
                    return 0;
                } else if (a.name > b.name) {
                    return 1;
                }
            });

            title = <h3>Connectors:</h3>;

            connectors = Array.isArray(this.props.connectors) && this.props.connectors.map(function(connectorDescription) {
                return <ConnectorDescription connector={connectorDescription}/>;
            });

        }

        return (
            <div className="connectors-description">
                {title}
                {connectors}
            </div>
        );
	}
});

var ConnectorDescription = React.createClass({

	render: function() {

        var connectorDetails = [],
            cssClass = 'connector-description',
            name = this.props.connector.name.replace('_', ' ');

        connectorDetails.push(<div className="connector-name">{name}</div>);

        if (this.props.connector.type) {

            connectorDetails.push(<div className="connector-type">{this.props.connector.type}</div>);
            cssClass += ' ' + this.props.connector.type;

        }

        if (this.props.connector.description) {
            connectorDetails.push(<div className="connector-description-text">{this.props.connector.description}</div>);
        }

        return (
            <div className={cssClass}>
                {connectorDetails}
            </div>
        );
	}
});

module.exports = ConnectorsDescription;
