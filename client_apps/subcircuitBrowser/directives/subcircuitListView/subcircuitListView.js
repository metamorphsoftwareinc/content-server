/**
 * Created by Robert Boyles on 5/28/15.
 */

/*global angular*/

'use strict';


require('..//subcircuitDetails/subcircuitDetails.jsx');
require('../../services/subcircuitLibrary.js');
require('../../../componentBrowser/directives/downloadButton/downloadButton.js');
require('../../../componentBrowser/directives/showMoreButton/showMoreButton.js');
require('../../../componentBrowser/directives/showLessButton/showLessButton.js');
require('../../../componentBrowser/directives/infoButton/infoButton.js');

var listViewBase = require('../../../common/listViewBase.js');

angular.module('mms.subcircuitBrowser.subcircuitListView', [
    'isis.ui.itemList',
    'mms.subcircuitBrowser.subcircuitLibrary',
    'mms.componentBrowser.downloadButton',
    'mms.componentBrowser.showMoreButton',
    'mms.componentBrowser.showLessButton',
    'mms.componentBrowser.infoButton',
    'mms.subcircuitDetails.react'
])
    .controller('SubcircuitListViewItemController', function () {
        //debugger;
    })

    .directive('subcircuitListView', function () {

        function SubcircuitDetailsController($scope) {

            var commonList = listViewBase.call(this, $scope, this.contentLibraryService);

            var config = commonList.config,
                self,
                renderList;

            self = this;

            this.listData = {
                items: []
            };

            if (typeof this.onItemDragStart === 'function' &&
                typeof this.onItemDragEnd === 'function') {

                config.onItemDragStart = function(e, item) {
                    self.onItemDragStart(e, item);
                };

                config.onItemDragEnd = function(e, item) {
                    self.onItemDragEnd(e, item);
                };

            }

            this.config = config;

            renderList = function () {

                var comps = [];

                for (var i in self.components.sort(function (a, b) {
                    return a.name.localeCompare(b.name);
                })) {

                    var comp = self.components[i],
                        item = commonList.itemGenerator(comp, 'subcircuit', '/subcircuitBrowser/templates/');

                    item.expandDetails = self.expandDetails;

                    comps.push(item);
                }
                self.listData.items = comps;

            };

            $scope.$watchCollection(function () {
                return self.components;
            }, function () {
                renderList();
            });

            renderList();

            this.expandDetails = function(item) {

                self.contentLibraryService.getDetails(item.id, item.details)
                    .then(function(documentation) {

                        item.details.documentation = documentation;
                        item.expandedDetails = true;

                    });

            };

        }


        return {
            restrict: 'E',
            replace: true,
            controller: SubcircuitDetailsController,
            controllerAs: 'ctrl',
            bindToController: true,
            templateUrl: '/subcircuitBrowser/templates/subcircuitListView.html',
            scope: {
                components: '=',
                onItemDragStart: '=',
                onItemDragEnd: '=',
                noDownload: '=',
                contentLibraryService: '='
            }
        };
    });
