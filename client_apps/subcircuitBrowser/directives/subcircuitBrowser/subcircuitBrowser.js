/*global angular*/

'use strict';


require('../subcircuitCategories/subcircuitCategories.js');
require('../../../componentBrowser/directives/componentSearch/componentSearch.js');
require('../subcircuitListing/subcircuitListing.js');
require('../../services/subcircuitLibrary.js');
require('../../../componentBrowser/directives/categoryResizer/categoryResizer.js');
require('../../../componentBrowser/services/componentLibrary.js');


angular.module('mms.subcircuitBrowser', [

    'mms.subcircuitBrowser.subcircuitCategories',

    'mms.componentBrowser.componentSearch',
    'mms.componentBrowser.componentLibrary',
    'mms.subcircuitBrowser.subcircuitListing',
    'mms.componentBrowser.templates',
    'mms.subcircuitBrowser.templates',
    'mms.subcircuitBrowser.subcircuitLibrary',
    'mms.contentBrowser.categoryResizer',
    'ngCookies'

])

    .directive('subcircuitBrowser', function () {

        function SubcircuitBrowserController ($scope, componentLibrary, subcircuitLibrary, $log, $anchorScroll, $timeout, $cookies, $location) {

            var self,
                updateList,
                noSearchResults,
                loadState;

            self = this;

            this.appBooting = false;

            this.showHeader = false;

            this.persistState = false;

            this.persistStateInUrl = false;

            this.selectedCategory = null;

            this.errorMessage = null;

            this.filtered = false;

            this.resultsForSearchText = null;

            this.componentsToList = null;

            this.searchText = null;

            this.facetedSearch = null;  // the json for faceted search

            this.columnSearchText = {};

            this.listingView = 'ListView';

            this.columnSortInfo = [];

            this.lockGridColumns = false;  // faceted search locks grid columns so they don't jump around while you are searching

            this.setFacetedSearch = function (fs) {
                self.facetedSearch = fs;
            };

            this.onCategorySelectionChange = function() {

                self.searchText = null;
                self.resultsForSearchText = null;

                self.pagingParameters.cursor = 0;
                self.filtered = false;

                $anchorScroll();

                updateList();

                self.saveState();

            };

            this.pagingParameters = {
                itemsPerPage: 20,
                cursor: 0
            };

            this.setItemsPerPage = function (ipp) {
                self.pagingParameters.itemsPerPage = ipp;
                updateList();
            };

            this.getNextPage = function() {

                var nextCursor;

                // console.log('next');

                nextCursor = self.pagingParameters.cursor + self.pagingParameters.itemsPerPage;

                if (nextCursor < self.pagingParameters.totalCount) {

                    self.pagingParameters.cursor = nextCursor;
                    updateList();

                }

                self.saveState();

            };

            this.getPrevPage = function() {

                // console.log('prev');

                self.pagingParameters.cursor = Math.max(
                    self.pagingParameters.cursor - self.pagingParameters.itemsPerPage,
                    0
                );

                updateList();

                self.saveState();
            };

            this.newData = function (results) {
                // console.log('Search results', results);
                if (results.subcircuit.length === 0 && self.facetedSearch === null) {
                    noSearchResults();

                    self.saveState();

                    return;
                }
                self.componentsToList = results.subcircuit;

                self.pagingParameters.cursor = 0;
                self.pagingParameters.fromNumber = 1;
                self.pagingParameters.totalCount = results.total;

                self.pagingParameters.toNumber = Math.min(
                    self.pagingParameters.cursor + self.pagingParameters.itemsPerPage,
                    self.pagingParameters.totalCount
                );

                self.filtered = true;
                self.resultsForSearchText = self.searchText;

                self.saveState();

            };

            this.getSearchResults = function () {
                subcircuitLibrary.searchSubCircuits(
                    self.selectedCategory && self.selectedCategory.path || '!',
                    self.searchText,
                    self.pagingParameters.itemsPerPage,
                    self.pagingParameters.cursor,
                    self.facetedSearch
                ).then(self.newData)
                    .catch(function(e) {
                        $log.warn('No results:', e);
                        self.componentsToList = null;
                        noSearchResults();
                    });
            };


            loadState = function() {

                var state,
                    locationSearchObject;

                if (self.persistState && $cookies.subcircuitBrowserState) {

                    state = JSON.parse($cookies.subcircuitBrowserState);

                    angular.extend(self, state);

                }

                if (self.persistStateInUrl) {

                    locationSearchObject = $location.search();

                    if (locationSearchObject && typeof locationSearchObject.s === 'string') {

                        state = JSON.parse(locationSearchObject.s );

                        angular.extend(self, state);

                    }

                }

            };

            this.onListingViewSelection = function(view) {

                self.listingView = view;
                self.saveState();

            };

            this.saveState = function() {

                var state = {};

                if (this.persistState || this.persistStateInUrl) {

                    //state.searchText = this.searchText; // TODO: make this work
                    //state.pagingParameters = this.pagingParameters; // TODO: make this work
                    //state.columnSearchText = this.columnSearchText; // TODO: make this work

                    state.selectedCategory = this.selectedCategory; // TODO: make tree navigate to category
                    state.listingView = this.listingView;


                    var stateObjectJSON = JSON.stringify(state);

                    if (this.persistState) {
                        $cookies.subcircuitBrowserState = stateObjectJSON;
                    }

                    if (this.persistStateInUrl) {
                        $location.search({ s: stateObjectJSON });
                    }

                }

            };

            noSearchResults = function() {
                self.pagingParameters.cursor = 0;
                self.filtered = false;
                self.componentsToList = null;
                self.errorMessage = 'No search results for \"' + self.searchText + '\"';
            };

            updateList = function () {

                self.errorMessage = null;

                if (self.selectedCategory || self.filtered) {

                    if (!self.filtered) {

                        angular.extend(self.pagingParameters, {

                            fromNumber: self.pagingParameters.cursor + 1,
                            toNumber: Math.min(
                                self.pagingParameters.cursor + self.pagingParameters.itemsPerPage,
                                self.selectedCategory.childComponentsCount
                            ),
                            totalCount: self.selectedCategory.childComponentsCount

                        });

                        subcircuitLibrary.getListOfSubCircuits(
                            self.selectedCategory.path,
                            self.pagingParameters.itemsPerPage,
                            self.pagingParameters.cursor
                        )
                            .then(function (data) {
                                //console.log('componentsToList: ', data);
                                $timeout(function(){ self.componentsToList = data.subcircuit; });
                                self.pagingParameters.totalCount = data.total;
                                self.pagingParameters.toNumber = Math.min(
                                    self.pagingParameters.cursor + self.pagingParameters.itemsPerPage,
                                    self.pagingParameters.totalCount
                                );
                            })
                            .catch(function (e) {
                                $log.error('Components could not be loaded', e);
                            });
                    } else {

                        angular.extend(self.pagingParameters, {

                            fromNumber: self.pagingParameters.cursor + 1,
                            toNumber: Math.min(
                                self.pagingParameters.cursor + self.pagingParameters.itemsPerPage,
                                self.pagingParameters.totalCount
                            )

                        });

                        subcircuitLibrary.searchSubCircuits(
                            self.selectedCategory && self.selectedCategory.path || '!',
                            self.searchText,
                            self.pagingParameters.itemsPerPage,
                            self.pagingParameters.cursor,
                            self.facetedSearch,
                            self.columnSortInfo
                        ).then(function(results) {

                                // console.log('Search results', results);
                                if (results.subcircuit.length === 0 && self.facetedSearch === null) {
                                    noSearchResults();
                                    return;
                                }

                                self.componentsToList = results.subcircuit;

                                self.pagingParameters.toNumber = Math.min(
                                    self.pagingParameters.cursor + self.pagingParameters.itemsPerPage,
                                    self.pagingParameters.totalCount
                                );

                                self.filtered = true;

                            })
                            .catch(function(e) {
                                $log.warn('No results:', e);
                                noSearchResults();
                            });

                    }
                }

            };

            // $scope.$watch(function () {
            //    return self.categoryToList;
            // }, function (newValue, oldValue) {

            //    if (newValue && newValue !== oldValue) {
            //        update();
            //    }

            // });
            
            this.checkComponentServerUp = function() {
                componentLibrary.getClassificationTree().then(
                    function () {

                            self.appBooting = false;

                        },
                        function (data) {

                            if ( parseInt(data.status, 10) === 503 || parseInt(data.status, 10) === 404) {
                                self.appBooting = true;
                            }

                        });
            };

            this.init = function() {

                loadState();
                this.getSearchResults();
                updateList();
                this.checkComponentServerUp();

            };

        }

        return {
            restrict: 'E',
            scope: {
                selectedView: '=',
                onItemDragStart: '=',
                onItemDragEnd: '='
            },
            replace: true,
            controller: SubcircuitBrowserController,
            bindToController: true,
            controllerAs: 'ctrl',
            templateUrl: '/subcircuitBrowser/templates/subcircuitBrowser.html',
            require: 'subcircuitBrowser',
            link: function(scope, element, attributes, ctrl) {

                if (attributes.hasOwnProperty('embedded')) {
                    ctrl.embedded = true;
                } else {
                    ctrl.embedded = false;
                }

                if (attributes.hasOwnProperty('persistState')) {
                    ctrl.persistState = true;
                } else {
                    ctrl.persistState = false;
                }

                if (attributes.hasOwnProperty('persistStateInUrl')) {
                    ctrl.persistStateInUrl = true;
                } else {
                    ctrl.persistStateInUrl = false;
                }

                if (attributes.hasOwnProperty('noDownload')) {
                    ctrl.noDownload = true;
                } else {
                    ctrl.noDownload = false;
                }
                
                var itemsPerPage = parseInt(attributes.listViewItemsPerPage, 10);

                if (!isNaN(itemsPerPage)) {
                    ctrl.pagingParameters.itemsPerPage = itemsPerPage;
                }

                ctrl.init();

            }
        };
    });
