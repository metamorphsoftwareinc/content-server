/*global angular*/

/**
 * Created by Blake McBride on 2/9/15.
 */


'use strict';

require('../../services/subcircuitLibrary.js');

angular.module('mms.subcircuitBrowser.subcircuitCategories', [
    'isis.ui.treeNavigator',
    'mms.subcircuitBrowser.subcircuitLibrary'
])

.directive('subcircuitCategories', function() {

        function SubcircuitCategoriesController($q, subcircuitLibrary, $timeout) {

            var self = this;

            var addData = function (parent, array) {
                var children = [];
                for (var i in array) {
                    var e = array[i];
                    var n = addNode(parent, e.label, e.id, i, e);
                    children.push(n);
                    if (e.subClasses !== undefined) {
                        addData(n, e.subClasses);
                    }
                }
                return children;
            };

            var config,
                treeNodes = {},
                addNode;

            addNode = function (parentTreeNode, lbl, id, i, e) {

                var newTreeNode,
                    children = [];

                // node structure
                newTreeNode = {
                    id: id,
                    label: lbl,
                    extraInfo: e && !isNaN(e.categoryTotal) && ( '[' + e.categoryTotal + ']' ),
                    children: children,
                    childrenCount: e === undefined ? 0 : e.childCategoriesCount,
                    nodeData: {
                        label: lbl,
                        path: e === undefined ? '' : e.id,
                        childComponentsCount: e === undefined ? 0 : e.childComponentsCount
                    },
                    iconClass: null,

                    draggable: false,
                    order: i
                };

                // add the new node to the map
                treeNodes[newTreeNode.id] = newTreeNode;


                if (parentTreeNode) {
                    // if a parent was given add the new node as a child node
                    parentTreeNode.iconClass = undefined;
                    parentTreeNode.children.push(newTreeNode);


                    //                parentTreeNode.childrenCount = parentTreeNode.children.length;

                    if (newTreeNode.childrenCount) {
                        newTreeNode.iconClass = undefined;
                    }

                    //               sortChildren( parentTreeNode.children );

                    newTreeNode.parentId = parentTreeNode.id;
                } else {

                    // if no parent is given replace the current root node with this node
                    self.treeData = newTreeNode;
                    self.treeData.unCollapsible = true;
                    newTreeNode.parentId = null;
                }

                return newTreeNode;
            };

            config = {

                scopeMenu: [{
                    items: []
                }],

                loadChildren: function (e, node) {
                    // console.log('loadChildren called:', node);
                    var deferred = $q.defer();

                    subcircuitLibrary.getSubCircuitTree(node.nodeData.path).then(
                        function (data) {

                            var children;

                            children = addData(node, data);
                            deferred.resolve(children);
                            self.appBooting = false;

                        },
                        function (data) {

                            if ( parseInt(data.status, 10) === 503 ) {
                                self.appBooting = true;
                            }

                        });

                    return deferred.promise;
                },

                showRootLabel: false,

                // Tree Event callbacks

                nodeClick: function (e, node) {

                    self.lockGridColumns = false;
                    console.log('clicked');

                    if (!self.selectedCategory ||
                        self.selectedCategory.path !== node.nodeData.path) {

                        self.selectedCategory = node.nodeData;

                        if (angular.isFunction(self.onSelectionChange)) {

                            $timeout(
                                self.onSelectionChange
                            );
                        }

                    }
                },

                nodeDblclick: function (e, node) {
                    console.log('Node was double-clicked:', node);
                },

                nodeExpanderClick: function (e, node, isExpand) {
                    console.log('Expander was clicked for node:', node, isExpand);
                }

            };

            self.config = config;
            //self.config.disableManualSelection = true;
            self.config.selectedScope = self.config.scopeMenu[0].items[0];
            self.config.nodeClassGetter = function (node) {
                var nodeCssClass = '';

                if (node.order % 2 === 0) {
                    nodeCssClass = 'even';
                }

                return nodeCssClass;
            };
            self.treeData = {};
            self.config.state = {
                // id of activeNode
                activeNode: 'Node item 0.0',

                // ids of selected nodes
                selectedNodes: ['Node item 0.0'],

                expandedNodes: ['Node item 0', 'Node item 0.1'],

                // id of active scope
                activeScope: 'project'
            };


            addNode(null, 'Components', 'Components');
        }


        return {
            restrict: 'E',
            controller: SubcircuitCategoriesController,
            controllerAs: 'ctrl',
            bindToController: true,
            templateUrl: '/subcircuitBrowser/templates/subcircuitCategories.html',
            scope: {
                selectedCategory: '=',
                onSelectionChange: '=',
                lockGridColumns: '=',
                appBooting: '='
            }
        };
    });
