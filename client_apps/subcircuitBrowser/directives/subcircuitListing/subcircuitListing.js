/**
 * Created by Blake McBride on 2/23/15.
 */

/*global angular, alert*/

'use strict';

require('../subcircuitListView/subcircuitListView.js');
//require('../gridView/gridView.js');
require('../../../componentBrowser/directives/countDisplay/countDisplay.js');
//require('../../../componentBrowser/directives/viewSelection/viewSelection.js');
require('../../../componentBrowser/directives/paging/paging.js');
require('../../services/subcircuitLibrary.js');

angular.module('mms.subcircuitBrowser.subcircuitListing', [
    'mms.subcircuitBrowser.subcircuitListView',
//    'mms.componentBrowser.gridView',
//    'mms.componentBrowser.viewSelection',
    'mms.componentBrowser.countDisplay',
    'mms.componentBrowser.paging',
    'mms.subcircuitBrowser.subcircuitLibrary'

])

.directive('subcircuitListing', function(subcircuitLibrary) {

    function SubcircuitListingController() {

        var self = this;

        this.selectedView = 'ListView';

        this.contentLibraryService = subcircuitLibrary;

        this.onViewSelection = function(view) {

            if (typeof self.onListingViewSelection === 'function') {
                self.onListingViewSelection({
                    view: view
                });
            }


        };

    }


    return {
        restrict: 'E',
        controller: SubcircuitListingController,
        controllerAs: 'ctrl',
        bindToController: true,
        scope: {
            componentsToList: '=',
            pagingParameters: '=',
            getNextPage: '=',
            getPrevPage: '=',
            selectedCategory: '=',
            searchText: '=',
            columnSearchText: '=',
            columnSortInfo: '=',
            newData: '=',
            facetedSearch: '=',
            setFacetedSearch: '=',
            lockGridColumns: '=',
//            selectedView: '=',
            onListingViewSelection: '&',
            onItemDragStart: '=',
            onItemDragEnd: '=',
            noDownload: '=',
            setItemsPerPage: '=',
            resultsForSearchText: '=',
            appBooting: '='
        },
        replace: true,
        templateUrl: '/subcircuitBrowser/templates/subcircuitListing.html'
    };
});
