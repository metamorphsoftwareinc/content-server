/*globals angular*/

'use strict';

angular.module(
    'mms.subcircuitBrowser.subcircuitLibrary', [])
    .provider('subcircuitLibrary', function SubcircuitLibraryProvider() {
        var serverUrl;

        this.setServerUrl = function (url) {
            serverUrl = url;
        };

        this.$get = [

            '$http',
            '$q',
            '$log',

            function ($http, $q, $log) {

                var SubcircuitLibrary,
                    encodeCategoryPath;

                encodeCategoryPath = function(path) {
                    return path.replace(/\//g, '!');
                };

                SubcircuitLibrary = function () {

                    var subcircuitTree,
                        subcircuitGrandTotal;

                    this.getServerUrl = function() {
                        return serverUrl;
                    };

                    this.getListOfSubCircuits = function(categoryPath, itemCount, cursor ) {

                        var deferred = $q.defer();

                        $http.get(serverUrl + '/subcircuit/search' + '/' + encodeCategoryPath(categoryPath) +
                            '/' + '_all' + '/' + itemCount + '/' + cursor)

                            .success(function (data) {
                                deferred.resolve(data);
                            })
                            .error(function (e) {    // assume attempt to page past available components
                                deferred.reject('Could not load list of sub-circuits', e);
                        });

                        return deferred.promise;
                    };


                    this.getSubCircuitTree = function (id) {

                        var deferred = $q.defer(),
                            url;

                        url = serverUrl + '/subcircuit/classification/tree';

                        if (angular.isString(id)) {
                            url += '/' + encodeCategoryPath(id);
                        }

                        $http.get(url)

                            .success(function (data) {

                                subcircuitTree = data.classes;
                                deferred.resolve(subcircuitTree);
                                subcircuitGrandTotal = data.grandTotal;

                            })
                            .error(function (data, status, headers, config) {
                                deferred.reject({
                                    msg: 'Could not load sub-circuit classification tree',
                                    data: data,
                                    status: status,
                                    headers: headers,
                                    config: config
                                });
                            });

                        return deferred.promise;
                    };

                    var downloadUrl = function (url) {
                        var hiddenIFrameID = 'hiddenDownloader',
                            iframe = document.getElementById(hiddenIFrameID);
                        if (iframe === null) {
                            iframe = document.createElement('iframe');
                            iframe.id = hiddenIFrameID;
                            iframe.style.display = 'none';
                            document.body.appendChild(iframe);
                        }
                        iframe.src = url;
                    };

                    this.downloadItem = function(id) {

                        $log.debug('Download handler');

                        downloadUrl(serverUrl + '/subcircuit/getsubcircuit/download/' + id);
                    };

                    this.getSubcircuitGrandTotal = function() {
                        return subcircuitGrandTotal;
                    };

                    this.searchSubCircuits = function(categoryPath, globalSearchText, itemCount, cursor) {

                        // console.log(serverUrl + '/subcircuit/search' + '/' + encodeCategoryPath(categoryPath) + '/' + globalSearchText + '/' + itemCount + '/' + cursor);

                        var deferred = $q.defer();

                        globalSearchText = globalSearchText === undefined || globalSearchText === null || globalSearchText === '' ? '_all' : globalSearchText;

                        // console.log(globalSearchText);

                        $http({
                            url: serverUrl + '/subcircuit/search' + '/' + encodeCategoryPath(categoryPath) + '/' + globalSearchText + '/' + itemCount + '/' + cursor,
                            method: 'GET'
                        })
                            .success(function(data){

                                if (data && angular.isArray(data.subcircuit) && data.subcircuit.length) {
                                    deferred.resolve(data);
                                } else {
                                    data = {};
                                    data.subcircuit = [];  //  no data
                                    deferred.resolve(data);
                                }

                            })
                            .error(function (e) {
                                deferred.reject('Could not perform search', e);
                            });

                        return deferred.promise;

                    };

                    this.getDetails = function (id) {

                        var path = serverUrl + '/subcircuit/overview/' + id,
                            deferred = $q.defer();


                        $http.get(path)

                            .then(function (json) {

                                var documentation = {
                                    id: id,
                                    description: json.data.description,
                                    connectors: json.data.connectors,
                                    visuals: json.data.visuals,
                                    icon: json.data.icon
                                };

                                deferred.resolve(documentation);

                            });

                        return deferred.promise;
                    };


                };

                return new SubcircuitLibrary();

            }
        ];
    });
