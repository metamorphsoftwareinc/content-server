'use strict';

require('../componentBrowser/services/componentLibrary.js');
require('../subcircuitBrowser/services/subcircuitLibrary.js');

module.exports = function ($scope, contentLibraryService) {

    var self,
        config,
        formatProperties,
        itemGenerator;

    self = this;

    config = {

        sortable: false,
        secondaryItemMenu: false,
        detailsCollapsible: false,
        showDetailsLabel: 'Show details',
        hideDetailsLabel: 'Hide details',
        noDownload: self.noDownload,

        // Event handlers

        itemSort: function (jQEvent, ui) {
            console.log('Sort happened', jQEvent, ui);
        },

        itemClick: function (event, item) {
            console.log('Clicked: ' + item);
        },

        itemDownload: function (event, item) {
            contentLibraryService.downloadItem(item.id);
        },

        itemInfo: function (event, item) {

            var url,
                win;

            if (item.octopart) {
                url = 'http://octopart.com/search?q=' + item.octopart + '&view=list';
            } else if (item.subcircuitSourceURL){
                url = item.subcircuitSourceURL;
            }

            if (url) {

                win = window.open(url, '_blank');
                win.focus();

            }

        },

        itemContextmenuRenderer: function (e, item) {

            var menu;

            if (!self.noDownload) {

                menu = [{
                    items: [

                        {
                            id: 'download',
                            label: 'Download item',
                            disabled: false,
                            action: function() {
                                contentLibraryService.downloadItem(item.id);
                            },
                            actionData: item,
                            iconClass: 'fa fa-plus'
                        }
                    ]
                }];

            }

            return menu;

        },

        detailsRenderer: function (item) {
            item.details = 'My details are here now!';
        }

    };

    formatProperties = function (item, itemClass) {
        var res = [],
            properties = {},
            pp, i, prop, key;

        if (item[itemClass + "Properties"] === undefined) {
            pp = item.prominentProperties;
            //  add non-prominent properties
            if (item.otherProperties !== undefined && item.otherProperties !== null) {
                if (pp === undefined || pp === null) {
                    pp = item.otherProperties;
                } else {
                    pp = pp.concat(item.otherProperties);
                }
            }

            if (pp !== undefined && pp !== null) {
                for (i in pp) {
                    prop = pp[i];
                    if (prop.name !== undefined && prop.value !== undefined) {
                        if (prop.name !== capitalizeFirstLetter(itemClass) + 'Name') {
                            properties[prop.name] = prop.value;
                            if (prop.units !== undefined) {
                                properties[prop.name] += ' ' + prop.units;
                            }
                        }
                    } else {
                        for (key in prop) {
                            if (key !== 'id') {
                                properties[prop.name] = prop[key];
                            }
                        }
                    }
                }
            }
        }
        else {
            pp = item[itemClass + "Properties"];
            if (pp !== undefined && pp !== null) {
                for (i in pp) {
                    prop = pp[i];
                    if (prop.name !== undefined && prop.stringValue !== undefined) {
                        if (prop.name !== capitalizeFirstLetter(itemClass) + 'Name') {
                            properties[prop.name] = prop.stringValue;
                            if (prop.units !== undefined) {
                                properties[prop.name] += ' ' + prop.units;
                            }
                        }
                    }
                }
            }
        }

        var sortedPropKeys = Object.keys(properties).sort(),
            j;

        for ( j = 0; j < sortedPropKeys.length; j++ ) {

            res.push( {
                name: sortedPropKeys[j],
                value: properties[sortedPropKeys[j]]
            });

        }

        return res;

    };

    itemGenerator = function (item, itemClass, templateUrlBase) {

        var details = {
            properties: formatProperties(item, itemClass),
            markdown: null,
            documentation: {
                id: item.id,
                description: item.description,
                connectors: item.connectors,
                visuals: null,
                icon: null
            }
        };

        if (item.iconFileName) {
            details.documentation.icon =
                 contentLibraryService.getServerUrl() + '/component_files/' + item.iconFileName;

        }


        return {
            id: item.id,
            title: item.name.replace(/_/g, ' '),
            toolTip: 'Open item',
            headerTemplateUrl: templateUrlBase + 'itemHeader.html',
            detailsTemplateUrl: templateUrlBase + 'itemDetail.html',
            details: details,
            subcircuitSourceURL: item.subcircuitSourceURL
        };
    };

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    return { config: config,
             itemGenerator: itemGenerator};
};
