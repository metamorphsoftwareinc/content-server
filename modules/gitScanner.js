/**
 * Created by Blake McBride on 1/7/15.
 */


'use strict';


var fs = require('fs');
var path = require('path');
var utils = require('../utils/objectUtils');
var esUtils = require('../utils/esUtils');
var app = require('../app');
var errLog = require('../utils/errorReporter');
var gitUtils = require('../utils/gitUtils');
var cacheUtils = require('../utils/cacheUtils');
var nu = require('../utils/numericUtils');


var savedComponentsFilename = 'components.json';  // file where cached components are saved

exports.inScan = false;  // is the scanner currently running?

var esIndex;    //  name of real index (not alias)


var whenDoneCallback;
var rootPath;
var components;  // local version
var nComponents;

var badIDs = {};  // ones with duplicate ID's
var nDupFiles = 0;  //  number of ACM files with duplicate ID's

var stack;
var startTime;
var changedAgain = false;

var repoChanged = function (event, filename) {
    if (!exports.inScan) {
        errLog.logError('Component library changed; commencing background re-scan.', null);
        exports.readComponents(rootPath);
    } else {
        changedAgain = true;
    }
};

exports.disableCache = true;

var saveComponents = function () {
    cacheUtils.writeCacheFile(components.idIndex, savedComponentsFilename, '_components', function () {
        console.log('Component cache saved to ' + savedComponentsFilename);
    });
};

var loadComponents = function (filename, callback) {
    cacheUtils.loadCacheFile(filename, '_components', function (comp) {
        if (comp) {
            exports.components = comp;
            console.log('Component cache loaded from ' + filename);
        }
        callback(comp);
    });
};

var makeComponentPath = function (filename) {
    var p;
    if (rootPath === null) { // during testing
        p = filename;
    } else {
        p = filename.substring(rootPath.length + 1);
    }
    return p.substring(0, p.lastIndexOf('/'));  // don't use path.sep here!
};

var toNiceName = function (name) {
    name = name.replace(/_/g, ' ');
    return name.charAt(0).toUpperCase() + name.slice(1);
};

var getPropertiesProminent = function (properties) {
    var res = [];
    var pi;
    for (pi in properties) {
        var prop = properties[pi];
        if (prop.$.OnDataSheet === 'true') {
            var obj = {};
            obj.name = toNiceName(prop.$.Name);
//            obj.id = prop.$.ID;
            if (0 === obj.name.toLowerCase().indexOf('octo')) {
                obj.value = prop.Value[0].ValueExpression[0].Value || '';
            } else {
                obj.value = nu.convertToEngineering((prop.Value[0].ValueExpression[0].Value || [])[0] || '');
            }
            obj.units = prop.Value[0].$.Unit;
            res[res.length] = obj;
        }
    }
    return res;
};

var getPropertiesOther = function (properties) {
    var res = [];
    var pi;
    for (pi in properties) {
        var prop = properties[pi];
        if (prop.$.OnDataSheet !== 'true') {
            var obj = {};
            obj.name = toNiceName(prop.$.Name);
//            obj.id = prop.$.ID;
            if (prop.Value !== undefined) {
                if (prop.Value[0] !== undefined && prop.Value[0].ValueExpression !== undefined && prop.Value[0].ValueExpression[0] !== undefined && prop.Value[0].ValueExpression[0].Value !== undefined) {
                    if (0 === obj.name.toLowerCase().indexOf('octo')) {
                        obj.value = prop.Value[0].ValueExpression[0].Value[0];
                    } else {
                        obj.value = nu.convertToEngineering(prop.Value[0].ValueExpression[0].Value[0]);
                    }
                }
                obj.units = prop.Value[0].$.Unit;
            }
            res[res.length] = obj;
        }
    }
    return res;
};

var getComponentProperties = function (properties) {
    var res = [];
    var pi;
    for (pi in properties) {
        var prop = properties[pi];
        var obj = {};
        obj.name = toNiceName(prop.$.Name);
//        obj.id = prop.$.ID;
        if (prop.Value !== undefined) {
            if (prop.Value[0] !== undefined && prop.Value[0].ValueExpression !== undefined && prop.Value[0].ValueExpression[0] !== undefined && prop.Value[0].ValueExpression[0].Value !== undefined) {
                if (0 === obj.name.toLowerCase().indexOf('octo')) {
                    obj.stringValue = prop.Value[0].ValueExpression[0].Value[0];
                } else {
                    obj.stringValue = nu.convertToEngineering(prop.Value[0].ValueExpression[0].Value[0]);
                }
                if (utils.isNumeric(prop.Value[0].ValueExpression[0].Value[0])) {
                    obj.numericValue = utils.toNumber(prop.Value[0].ValueExpression[0].Value[0]);
                }
            }
            obj.units = prop.Value[0].$.Unit;
        }
        res.push(obj);
    }
    return res;
};


var duplicateIDs = [];

var addToComponents2 = function (filename, acm, ii, start, files, datasheet) {
    var connectors, conarray, clen, j, cinfo, scon;

    var iconFileName = path.dirname(filename) + path.sep + 'Icon.svg';
    fs.exists(iconFileName, function (exists) {
        if (!exists) {
            iconFileName = undefined;
        } else {
            iconFileName = iconFileName.substr(rootPath.length + 1);
        }

        if (!iconFileName) {
            try {
                iconFileName = path.dirname(filename) + path.sep + 'Icon.png';
                var stats = fs.lstatSync(iconFileName);

                if (stats.isFile()) {
                    iconFileName = iconFileName.substr(rootPath.length + 1);
                }
            }
            catch (e) {
                iconFileName = undefined;
            }
        }

        var classifications = acm.Component.Classifications[0]._.split('.');
        var comp = components;
        var len = classifications.length;
        var handleReturn = function (err, response, status) {
            if (err !== undefined || status !== 201 || response.created !== true) {
                errLog.logError('Unexpected ES create response: ' + status + ' ' + (response || {}).created, err);
            }
        };
        for (var i = 0; i < len; i++) {
            var label = classifications[i];
            if (i + 1 === len) {
                if (comp[label] === undefined) {
                    comp[label] = {};
                }
                if (comp[label]._components === undefined) {
                    comp[label]._components = [];
                }
                var compArray = comp[label]._components;
                var compObj = {};
                compObj.name = toNiceName(acm.Component.$.Name);
                compObj.id = acm.Component.$.ID;
                filename = filename.replace(/\\/g, '/');  // for the benefit of testing on Windows
                compObj.path = makeComponentPath(filename);
                compObj.filename = filename;
                compObj.classificationLabels = classifications; //  classificationLabels(classifications);
                compObj.classifications = acm.Component.Classifications[0]._.replace(/\./g, '/');
                compObj.prominentProperties = getPropertiesProminent(acm.Component.Property);
                compObj.otherProperties = getPropertiesOther(acm.Component.Property);
                compObj.position = compArray.length;

                connectors = acm.Component.Connector || [];
                conarray = [];
                clen = connectors.length;
                for (j = 0; j < clen; j++) {
                    cinfo = connectors[j].$;
                    if (cinfo) {
                        scon = {};
                        scon.name = cinfo.Name;
                        scon.type = cinfo.Definition;
                        scon.description = cinfo.Notes;
                        conarray.push(scon);
                    }
                }
                compObj.connectors = conarray;

                if (iconFileName) {
                    compObj.iconFileName = iconFileName;
                }
                if (datasheet) {
                    compObj.datasheet = datasheet;
                }
                // hack so we don't retain the entire XML string. See also https://github.com/cheeriojs/cheerio/issues/263
                compObj = JSON.parse(JSON.stringify(compObj));
                compArray[compObj.position] = compObj;

                var doc = {};
                doc.name = compObj.name;
                doc.classifications = compObj.classifications + '/';
                doc.componentProperties = getComponentProperties(acm.Component.Property);
                if (iconFileName) {
                    doc.iconFileName = iconFileName;
                }
                if (datasheet) {
                    doc.datasheet = datasheet;
                }
                doc.position = compObj.position;
                doc.connectors = compObj.connectors;
                esUtils.addDoc(esIndex, 'component', compObj.id, doc, handleReturn);

                components.idIndex[compObj.id] = compObj;
            } else {
                if (comp[label] === undefined) {
                    comp[label] = {};
                }
                comp = comp[label];
            }
        }
        utils.nextFun(processFiles, ii + 1, start, files);
    });
};

var endsWith = function (str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

var addToComponents = function (filename, acm, ii, start, files) {
    var i, len;
    if (acm === undefined || acm.Component === undefined) {
        console.log('Bad acm file at: ' + filename)
        utils.nextFun(processFiles, ii + 1, start, files);
        return;
    }
    if (!acm.Component.Classifications || acm.Component.Classifications[0]._ === null || acm.Component.Classifications[0]._ === undefined) {
        acm.Component.Classifications = [{'_': 'Uncategorized'}];
    }

    if (components.idIndex[acm.Component.$.ID] !== undefined) {
        badIDs[acm.Component.$.ID] = true;
        console.log('Warning: duplicate component ID ' + acm.Component.$.ID + ' - ' + acm.Component.$.Name);
        nDupFiles++;
        duplicateIDs.push(acm.Component.$.ID);
        utils.nextFun(processFiles, ii + 1, start, files);
        return;
    }

    nComponents++;

    var rdFile, rdFiles = acm.Component.ResourceDependency;
    var datasheet = undefined;
    if (Array.isArray(rdFiles)) {
        len = rdFiles.length;
        for (i = 0; i < len; i++) {
            rdFile = rdFiles[i].$.Path;
            if (endsWith(rdFile, ".pdf") || endsWith(rdFile, ".PDF")) {
                datasheet = path.dirname(filename) + path.sep + rdFile;
                break;
            }
        }
    }
    if (datasheet !== undefined) {
        if (path.sep === '/') {
            datasheet = datasheet.replace(/\\/g, path.sep);
        }
        fs.exists(datasheet, function (exists) {
            if (!exists) {
                datasheet = undefined;
            } else {
                datasheet = datasheet.substr(rootPath.length + 1);
            }
            addToComponents2(filename, acm, ii, start, files, datasheet);
        });
    } else {
        addToComponents2(filename, acm, ii, start, files, datasheet);
    }
};

var finalActions = function () {
    exports.components = components;  // make components globally available
    exports.inScan = false;
    if (changedAgain) {
        changedAgain = false;
        utils.nextFun(repoChanged);
    } else if (whenDoneCallback !== undefined) {
        whenDoneCallback(components);
    }
};

var switchEsDB = function (err, name) {
    if (err === undefined && name !== undefined) {
        esUtils.changeAlias(name, esIndex, 'components', function (err, resp) {
            if (err !== undefined) {
                errLog.logError("Error renaming elasticsearch alias", err);
            }
            esUtils.deleteIndex(name);
            finalActions();
        });
    } else {
        errLog.logError("Severe Error:  can't obtain index real name", err);
        console.log('Stop the component server, stop elasticsearch, rm the elasticsearch files at /var/lib/elasticsearch, restart elasticsearch, restart server')
        finalActions();
    }
};

var handleEsDB = function (err, resp) {
    if (err === undefined) {
        if (resp) {
            esUtils.getIndexName('components', switchEsDB);
        } else {
            esUtils.createAlias(esIndex, 'components', function (err, resp) {
                if (err !== undefined) {
                    errLog.logError('Error creating elasticsearch alias', err);
                }
                finalActions();
            });
        }
    } else {
        finalActions();
    }
};

var addTotals = function (comp) {
    var n = 0;
    var catName;
    for (catName in comp) {
        if (catName !== 'idIndex' && catName !== '_components') {
            n += addTotals(comp[catName]);
        } else if (catName === '_components') {
            n += comp._components.length;
        }
    }
    comp.categoryTotal = n;
    return n;
};

var endOfScan = function () {
    var endTime;
    addTotals(components);
    endTime = new Date().getTime();
    console.log('Component scan complete.  ' + nComponents + ' components processed in ' + ((endTime - startTime) / 1000) + ' seconds.');
    saveComponents();
    if (nDupFiles !== 0) {
        console.log('Number of ACM files with duplicate ID\'s is: ' + nDupFiles);
//        utils.saveJsonSync(duplicateIDs, "duplicateIDs.json");
    }
    esUtils.indexExists('components', handleEsDB);
};

var processFiles = function (i, start, files) {
    if (i >= files.length) {
        if (stack.length > 0) {
            files = stack.pop();
            start = stack.pop();
            i = stack.pop();
            utils.nextFun(processFiles, i + 1, start, files);
        } else {
            //  This is where we end up when the scan is complete.
            endOfScan();
        }
    } else {
        var fileName = files[i];
        var fullPath = path.join(start, fileName);
        var afterStat = function (err, stat) {
            if (stat.isFile()) {
                if (path.extname(fileName) === '.acm') {
                    utils.readXml(fullPath, function (filename, acm) {
                        addToComponents(filename, acm, i, start, files);
                    });
                } else {
                    utils.nextFun(processFiles, i + 1, start, files);
                }
            } else {
                stack.push(i);
                stack.push(start);
                stack.push(files);
                utils.nextFun(readDir, fullPath);
            }
        };
        fs.stat(fullPath, afterStat);
    }
};

var readDir = function (start) {
    fs.readdir(start, function (err, files) {
        files = files.sort();
        processFiles(0, start, files);
    });
};

exports.initialize = function (rpath, callback) {
    var outOfDate = true;

    rootPath = path.normalize(rpath);

    if (app.autoRescan) {
        var gitIndex = gitUtils.getGitIndex(rootPath);

        if (gitIndex) {
            gitUtils.watchGitDir(gitIndex, repoChanged);
            console.log('component gitScanner is watching path: ' + gitIndex);
        } else {
            console.log('Could not find .git directory. Changes to components will not be reflected until restart');
        }

        try {
            outOfDate = fs.statSync(gitIndex).mtime > fs.statSync(savedComponentsFilename).mtime;
        } catch (err) {
            // ignore
        }
    }

    if (!exports.disableCache && !outOfDate && fs.existsSync(savedComponentsFilename)) {
        loadComponents(savedComponentsFilename, function () {
            if (exports.components) {
                console.log('Cache is available while the component library is scanned.');
            } else {
                console.log('Cache was invalid. Please wait while the component library is scanned.');
            }
            exports.readComponents(rpath, callback);
        });
    } else {
        console.log('Please wait while the component library is scanned.');
        exports.readComponents(rpath, callback);
    }
};

exports.readComponents = function (rpath, callback) {
    if (exports.inScan) {
        return;
    }
    exports.inScan = true;

    esIndex = esUtils.newName('components');

    var mapping = {
        'mappings': {
            'index_name': {
                'properties': {
                    'index_name': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    }
                }
            },
            'component': {
                'properties': {
                    'classifications': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    },
                    'name': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    },
                    'componentProperties': {
                        'type': 'nested',
                        'properties': {
                            'name': {
                                'type': 'string',
                                'index': 'not_analyzed'
                            },
                            'units': {
                                'type': 'string',
                                'index': 'not_analyzed'
                            },
                            'stringValue': {
                                'type': 'string'
                            },
                            'numericValue': {
                                'type': 'double'
                            }
                        }
                    },
                    connectors: {
                        properties: {
                            description: {type: "string"},
                            name: {type: "string"},
                            type: {type: "string"}
                        }
                    },
                    iconFileName: {type: "string", 'index': 'not_analyzed'},
                    datasheet: {type: "string", 'index': 'not_analyzed'},
                    'position': {
                        'type': 'long'
                    }
                }
            }
        }
    };
    esUtils.post(esIndex, mapping, function (err, resp) {
        if (err !== null) {
            errLog.logError('Error mapping elasticsearch index.', err);
        } else {
            esUtils.createIndex(esIndex, function (err, resp) {
                if (err !== undefined) {
                    errLog.logError('Error creating elasticsearch index.', err);
                } else {
                    whenDoneCallback = callback;
                    stack = [];
                    components = {};
                    nComponents = 0;
                    components.idIndex = {};
                    startTime = new Date().getTime();

                    readDir(rootPath);
                }
            });
        }
    });

};

