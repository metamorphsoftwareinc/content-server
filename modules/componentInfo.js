/**
 * Created by Blake McBride on 1/8/15.
 */


'use strict';


var gitScanner = require('./gitScanner');
var path = require('path');
var esUtils = require("../utils/esUtils");
var objUtils = require("../utils/objectUtils");


var numComponents = function (x) {
    if (x === null || x === '') {
        return 0;
    }
    return x.split('/').length;
};

var fixLabel = function (lbl) {
    lbl = lbl.replace(/_/g, ' ');
    return lbl.charAt(0).toUpperCase() + lbl.slice(1);
};

var buildComponentTree = function (fromPath, comps, compArray, orgArray, count, path, rootLevels) {
    for (var key in comps) {
        if (key === 'idIndex' || key === '_components') {
            continue;
        }
        var node = comps[key];
        var newnode = {};
        if (path === '') {
            newnode.id = key;
        } else {
            newnode.id = path + '/' + key;
        }
        newnode.label = fixLabel(key);
        if (node instanceof Array) {  // components
            var cArray;
            if (fromPath === newnode.id) {
                cArray = orgArray;
            } else {
                cArray = [];
            }
            for (var i = 0; i < node.length; i++) {
                var fcomp = node[i];
                var tcomp = {};
                tcomp.id = newnode.id + '/' + fcomp.name;
                tcomp.label = fcomp.name;
                tcomp.symbolUrl = tcomp.id + '/Icon.png';
                tcomp.childCategoriesCount = 0;
                tcomp.childComponentsCount = 0;
                cArray[cArray.length] = tcomp;
            }
            newnode.childCategoriesCount = 0;
            newnode.childComponentsCount = cArray.length;
            if (count > 1) {
                newnode.subClasses = cArray;
            }
        } else {  // category
            var subArray;
            if (newnode.id.indexOf(fromPath) === 0 && rootLevels === numComponents(newnode.id)) {
                subArray = orgArray;
            } else {
                subArray = [];
            }
            if (count > 0) {
                buildComponentTree(fromPath, node, subArray, orgArray, count - 1, newnode.id, rootLevels);
            }
            newnode.childCategoriesCount = subArray.length;
            if (node._components instanceof Array) {
                newnode.childComponentsCount = node._components.length;
            } else {
                newnode.childComponentsCount = 0;
            }
            if (count > 1 && subArray.length !== 0 && compArray !== subArray) {
                newnode.subClasses = subArray;
            }
        }
        if (newnode.id.indexOf(fromPath) === 0 && newnode.id.length !== fromPath.length) {
            compArray[compArray.length] = newnode;
        }
    }
};

var buildClassificationTree = function (fromPath, comps, compArray, orgArray, count) {
    var total = 0;
    for (var key in comps) {
        if (key === 'idIndex' || key === '_components' || key === 'categoryTotal') {
            continue;
        }
        var node = comps[key];
        var newnode = {};
        if (fromPath.length === 0) {
            newnode.id = key;
        } else {
            newnode.id = fromPath + '/' + key;
        }
        newnode.label = fixLabel(key);
        newnode.categoryTotal = node.categoryTotal;
        total += node.categoryTotal;
        var subArray = [];
        if (count > 0) {
            buildClassificationTree(newnode.id, node, subArray, orgArray, count - 1);
        }
        newnode.childCategoriesCount = subArray.length;
        if (node._components instanceof Array) {
            newnode.childComponentsCount = node._components.length;
        } else {
            newnode.childComponentsCount = 0;
        }
        if (count > 1 && subArray.length !== 0 && compArray !== subArray) {
            newnode.subClasses = subArray;
        }
        if (newnode.id.indexOf(fromPath) === 0 && newnode.id.length !== fromPath.length) {
            compArray[compArray.length] = newnode;
        }
    }
    return total;
};

exports.search = function (classification, searchText, cursor, size, columnSpecifics, sortColumns, callback) {
    var subSearch;
    var searchObj = {
        from: cursor,
        size: size,
        query: {
            bool: {
                must: []
            }
        }
    };

    (function setupSort() {
        if (sortColumns !== undefined && Array.isArray(sortColumns) && sortColumns.length > 0) {
            var index, sortIn, sortOut, fldType;
            searchObj.sort = [];
            for (index in sortColumns) {
                sortIn = sortColumns[index];
                sortOut = {};
                if (sortIn.type === 'numeric') {
                    fldType = 'componentProperties.numericValue';
                } else {
                    fldType = 'componentProperties.stringValue';
                }
                sortOut[fldType] = {
                    order: sortIn.sort,
                    'nested_filter': {
                        term: {
                            'componentProperties.name': sortIn.field
                        }
                    }
                };
                searchObj.sort.push(sortOut);
            }
        }
    })();

    var must = [];
    var subMust = [];
    var operator, num1, num2;
    var parseRange = function (str) {
        var args;
        if (typeof(str) !== 'string') {
            return false;
        }
        str = str.replace(/\s+/g, ' ').trim();
        if (str.indexOf('>=') === 0) {
            operator = 'gte';
            str = str.slice(2).trim();
        } else if (str.indexOf('<=') === 0) {
            operator = 'lte';
            str = str.slice(2).trim();
        } else if (str.indexOf('<') === 0) {
            operator = 'lt';
            str = str.slice(1).trim();
        } else if (str.indexOf('>') === 0) {
            operator = 'gt';
            str = str.slice(1).trim();
        }
        if (operator !== undefined) {
            if (objUtils.isNumeric(str)) {
                num1 = objUtils.toNumber(str);
                return true;
            }
            return false;
        } else { // possible range
            str = str.replace(/[eE]-/, 'e@');
            str = str.replace(/ /g, '');
            args = str.split('-');
            if (args.length !== 2) {
                return false;
            }
            args[0] = args[0].replace('e@', 'e-');
            args[1] = args[1].replace('e@', 'e-');
            if (!objUtils.isNumeric(args[0]) || !objUtils.isNumeric(args[1])) {
                return false;
            }
            operator = 'range';
            num1 = objUtils.toNumber(args[0]);
            num2 = objUtils.toNumber(args[1]);
            return true;
        }
    };

    if (classification !== '/') {
        must.push({
            match_phrase_prefix: {
                classifications: classification + '/'
            }
        });
    }
    if (searchText !== undefined && searchText !== null && searchText !== '' && searchText !== '_all') {
        must.push({
            match: {
                _all: searchText
            }
        });
    }
    must.push({match: {_type: "component"}});
    if (objUtils.countProperties(columnSpecifics) !== 0) {
        for (var key in columnSpecifics) {
            var val = columnSpecifics[key];
            var match;
            subSearch = {
                nested: {
                    path: 'componentProperties',
                    query: {
                        bool: {
                            should: [],
                            minimum_should_match: 1
                        }
                    }
                }
            };

            subSearch.nested.query.bool.must = {
                match: {'componentProperties.name': key}
            };

            if (objUtils.isNumeric(val)) {
                match = {match: {}};
                match.match['componentProperties.numericValue'] = objUtils.toNumber(val);
                subSearch.nested.query.bool.should.push(match);

                match = {match: {}};
                match.match['componentProperties.stringValue'] = val;
                subSearch.nested.query.bool.should.push(match);
            } else if (parseRange(val)) {
                match = {range: {}};
                if (operator === 'range') {
                    match.range['componentProperties.numericValue'] = {
                        'gte': num1,
                        'lte': num2
                    };
                } else {
                    var tmp = {};
                    tmp[operator] = num1;
                    match.range['componentProperties.numericValue'] = tmp;
                }
                subSearch.nested.query.bool.should.push(match);
            } else {
                match = {match: {}};
                match.match['componentProperties.stringValue'] = val;
                subSearch.nested.query.bool.should.push(match);
            }
            must.push(subSearch);
        }
    }
    searchObj.query.bool.must = must;
    esUtils.get('components/_search', searchObj, callback);
};

exports.isReady = function () {
    return !!gitScanner.components;
};

/* fromRoot is actually a relative path through the component tree  */
exports.componentTree = function (fromRoot, count) {
    var comps = gitScanner.components;
    var res = {};
    res.classes = [];
    if (fromRoot === null) {
        fromRoot = '';
    } else {
        fromRoot = fromRoot.replace(/!/g, '/');
        if (fromRoot.charAt(0) === '/') {
            fromRoot = fromRoot.substring(1);
        }
    }
    var rootLevels = numComponents(fromRoot);
    count += rootLevels;
    buildComponentTree(fromRoot, comps, res.classes, res.classes, count, '', rootLevels);
    return res;
};

/* fromRoot is actually a relative path through the component tree  */
exports.classificationTree = function (fromRoot, count) {
    var comps = gitScanner.components;
    var res = {};
    res.classes = [];
    fromRoot = fromRoot.replace(/!/g, '/');
    if (fromRoot.charAt(0) === '/') {
        fromRoot = fromRoot.substring(1);
    }
    var rootArray = fromRoot.length === 0 ? [] : fromRoot.split('/');
    for (var i = 0; i < rootArray.length; i++) {
        comps = comps[rootArray[i]];
    }
    res.grandTotal = buildClassificationTree(fromRoot, comps, res.classes, res.classes, count);
    return res;
};

exports.componentsList = function (clas, count, cursor) {
    var comps = gitScanner.components;
    var res = {};
    res.components = [];
    clas = clas.replace(/!/g, '/');
    if (clas.charAt(0) === '/') {
        clas = clas.substring(1);
    }
    var i, n, rootArray = clas.length === 0 ? [] : clas.split('/');
    for (i = 0; i < rootArray.length && comps[rootArray[i]] !== undefined; i++) {
        comps = comps[rootArray[i]];
    }
    comps = comps._components;
    if (comps instanceof Array) {
        for (i = cursor, n = count; i < comps.length && n > 0; i++, n--) {
            var fcomp = comps[i];
            var tcomp = {};
            tcomp.id = fcomp.id;
            tcomp.name = fcomp.name;
            tcomp.classificationLabels = fcomp.classificationLabels;
            tcomp.prominentProperties = fcomp.prominentProperties;
            tcomp.otherProperties = fcomp.otherProperties;
            tcomp.position = fcomp.position;  //  in this case, should always be the same as i
            res.components[res.components.length] = tcomp;
        }
    }
    return res;
};

exports.componentsOverview = function (id) {
    var comp = gitScanner.components.idIndex[id];
    var ret = {};
    if (comp !== undefined) {
        ret.id = comp.id;
        ret.name = comp.name;
        ret.classificationLabels = comp.classificationLabels;
        ret.prominentProperties = comp.prominentProperties;
        ret.otherProperties = comp.otherProperties;
        ret.position = comp.position;
        ret.connectors = comp.connectors;
        ret.iconFileName = comp.iconFileName;
        if (comp.datasheet !== undefined) {
            ret.datasheet = comp.datasheet;
        }
    }
    return ret;
};

exports.componentPath = function (id) {
    var comp = gitScanner.components.idIndex[id];
    if (comp !== undefined) {
        return path.dirname(comp.filename);
    }
    return undefined;
};

exports.acmPath = function (id) {
    var comp = gitScanner.components.idIndex[id];
    if (comp !== undefined) {
        return comp.filename;
    }
    return undefined;
};
