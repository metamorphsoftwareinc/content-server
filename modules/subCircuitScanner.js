/**
 * Created by Blake McBride on 5/11/15.
 */


'use strict';


var fs = require('fs');
var path = require('path');
var utils = require('../utils/objectUtils');
var esUtils = require('../utils/esUtils');
var app = require('../app');
var errLog = require('../utils/errorReporter');
var gitUtils = require('../utils/gitUtils');


var savedComponentsFilename = 'subcircuits.json';  // file where cached subcircuits are saved

exports.inScan = false;  // is the scanner currently running?

var esIndex;    //  name of real index (not alias)


var whenDoneCallback;
var rootPath;
var subcircuits;  // local version
var nSubCircuits;

var badIDs = {};  // ones with duplicate ID's
var nDupFiles = 0;  //  number of ADM files with duplicate ID's

var stack;
var startTime;
var changedAgain = false;

var repoChanged = function (event, filename) {
    if (!exports.inScan) {
        errLog.logError('Sub-circuit library changed; commencing background re-scan.', null);
        exports.readComponents(rootPath);
    } else {
        errLog.logError('Sub-circuit library changed; queuing re-scan.', null);
        changedAgain = true;
    }
};

exports.disableCache = true;

var saveComponents = function () {
    if (exports.disableCache !== true) {
        utils.saveJson(subcircuits, savedComponentsFilename);
        console.log('Sub-circuit cache saved to ' + savedComponentsFilename);
    }
};

var realLoadComponents = function (filename) {
    if (filename === undefined) {
        filename = savedComponentsFilename;
    }
    exports.subcircuits = subcircuits = JSON.parse(fs.readFileSync(filename, {encoding: 'utf8'}));
    console.log('Sub-circuit cache loaded from ' + filename);
    return subcircuits;
};

exports.loadComponents = function (filename) {
    // assume elastic already has the index
    //esUtils.deleteIndex('subcircuits', function(error, response) {
    realLoadComponents(filename);
};

var makeComponentPath = function (filename) {
    var p;
    if (rootPath === null) { // during testing
        p = filename;
    } else {
        p = filename.substring(rootPath.length + 1);
    }
    return p.substring(0, p.lastIndexOf('/'));  // don't use path.sep here!
};

var toNiceName = function (name) {
    name = name.replace(/_/g, ' ');
    return name.charAt(0).toUpperCase() + name.slice(1);
};

var duplicateIDs = [];

var addToComponents = function (filename, adm, ii, start, files) {
    var id, classificationsArray, comp, len, i, classificationsString, connectors, clen, j, cinfo, conarray, scon;
    if (adm === undefined || adm.Design === undefined || adm.Design.$ === undefined) {
        console.log('Bad adm file at: ' + filename);
        utils.nextFun(processFiles, ii + 1, start, files);
        return;
    }
    //if (adm.Design.Classifications[0]._ === null  ||  adm.Design.Classifications[0]._ === undefined) {
    //    //       console.log('Error: missing classification: ' + filename);
    //    utils.nextFun(processFiles, ii+1, start, files);
    //    return;
    //}
    id = adm.Design.$.DesignID.replace('{', '').replace('}', '');
    if (subcircuits.idIndex[id] !== undefined) {
        badIDs[id] = true;
        console.log('Warning: duplicate sub-circuit ID ' + id + ' - ' + adm.Design.$.Name);
        nDupFiles++;
        duplicateIDs.push(id);
        utils.nextFun(processFiles, ii + 1, start, files);
        return;
    }

    nSubCircuits++;

    classificationsArray = filename.substring(rootPath.length + 1).split(path.sep);
    if (classificationsArray.length > 3) {
        classificationsArray.splice(classificationsArray.length - 3, 3);
        classificationsString = classificationsArray.join('.');
    } else if (adm.Design.$.Classifications === undefined) {
        classificationsString = 'Unclassified';
        classificationsArray = ['Unclassified'];
    } else {
        classificationsString = adm.Design.$.Classifications[0];
        classificationsArray = classificationsString.split('.');
    }
    comp = subcircuits;
    len = classificationsArray.length;
    for (i = 0; i < len; i++) {
        var label = classificationsArray[i];
        if (i + 1 === len) {
            if (comp[label] === undefined) {
                comp[label] = {};
            }
            if (comp[label]._subcircuits === undefined) {
                comp[label]._subcircuits = [];
            }
            var compArray = comp[label]._subcircuits;
            var compObj = {};
            compObj.name = toNiceName(adm.Design.$.Name);
            compObj.id = id;
            filename = filename.replace(/\\/g, '/');  // for the benefit of testing on Windows
            compObj.path = makeComponentPath(filename);
            compObj.filename = filename;
            if (adm.Design.RootContainer && adm.Design.RootContainer[0]) {
                if (adm.Design.RootContainer[0].$) {
//                    compObj.name = adm.Design.RootContainer[0].$.Name;
                    compObj.description = adm.Design.RootContainer[0].$.Description;
                }
                connectors = adm.Design.RootContainer[0].Connector || [];
                conarray = [];
                clen = connectors.length;
                for (j = 0; j < clen; j++) {
                    cinfo = connectors[j].$;
                    if (cinfo) {
                        scon = {};
                        scon.name = cinfo.Name;
                        scon.type = cinfo.Definition;
                        scon.description = cinfo.Notes;
                        conarray.push(scon);
                    }
                }
                compObj.connectors = conarray;

                var subcircuitSourceUrl = (adm.Design.RootContainer[0].Property || []).filter(function (prop) {
                    return prop.$.Name.toLowerCase() === 'subcircuit source url'
                });
                if (subcircuitSourceUrl && subcircuitSourceUrl[0] && subcircuitSourceUrl[0].Value && subcircuitSourceUrl[0].Value[0] && subcircuitSourceUrl[0].Value[0].ValueExpression &&
                    subcircuitSourceUrl[0].Value[0].ValueExpression[0] && subcircuitSourceUrl[0].Value[0].ValueExpression[0].Value && subcircuitSourceUrl[0].Value[0].ValueExpression[0].Value[0]) {
                    compObj.subcircuitSourceURL = subcircuitSourceUrl[0].Value[0].ValueExpression[0].Value[0];
                }
            }
            compObj.classificationLabels = classificationsArray; //  classificationLabels(classifications);
            compObj.classifications = classificationsString.replace(/\./g, '/');
            compObj.position = compArray.length;
            compArray[compObj.position] = compObj;

            var doc = {};
            doc.name = compObj.name;
            doc.classifications = compObj.classifications + '/';
            doc.description = compObj.description;
            doc.position = compObj.position;
            esUtils.addDoc(esIndex, 'subcircuit', compObj.id, doc, function (err, response, status) {
                if (err !== undefined || status !== 201 || response.created !== true) {
                    errLog.logError('Unexpected ES create response: ' + status + ' ' + (response || {}).created, err);
                }
            });

            subcircuits.idIndex[compObj.id] = compObj;
        } else {
            if (comp[label] === undefined) {
                comp[label] = {};
            }
            comp = comp[label];
        }
    }
    utils.nextFun(processFiles, ii + 1, start, files);
};

var finalActions = function () {
    exports.subcircuits = subcircuits;  // make subcircuits globally available
    exports.inScan = false;
    if (changedAgain) {
        changedAgain = false;
        utils.nextFun(repoChanged);
    } else if (whenDoneCallback !== undefined) {
        whenDoneCallback(subcircuits);
    }
};

var switchEsDB = function (err, name) {
    if (err === undefined && name !== undefined) {
        esUtils.changeAlias(name, esIndex, 'subcircuits', function (err, resp) {
            if (err !== undefined) {
                errLog.logError('Error renaming elasticsearch alias', err);
            }
            esUtils.deleteIndex(name);
            finalActions();
        });
    } else {
        errLog.logError("Severe Error:  can't obtain index real name", err);
        console.log('Stop the component server, stop elasticsearch, rm the elasticsearch files at /var/lib/elasticsearch, restart elasticsearch, restart server')
        finalActions();
    }
};

var handleEsDB = function (err, resp) {
    if (err === undefined) {
        if (resp) {
            esUtils.getIndexName('subcircuits', switchEsDB);
        } else {
            esUtils.createAlias(esIndex, 'subcircuits', function (err, resp) {
                if (err !== undefined) {
                    errLog.logError("Error creating elasticsearch alias", err);
                }
                finalActions();
            });
        }
    } else {
        finalActions();
    }
};

var addTotals = function (comp) {
    var n = 0;
    var catName;
    for (catName in comp) {
        if (catName !== 'idIndex' && catName !== '_subcircuits') {
            n += addTotals(comp[catName]);
        } else if (catName === '_subcircuits') {
            n += comp._subcircuits.length;
        }
    }
    comp.categoryTotal = n;
    return n;
};

var endOfScan = function () {
    var endTime;
    addTotals(subcircuits);
    endTime = new Date().getTime();
    console.log('Sub-circuit scan complete.  ' + nSubCircuits + ' sub-circuits processed in ' + ((endTime - startTime) / 1000) + ' seconds.');
    saveComponents();
    if (nDupFiles !== 0) {
        console.log('Number of ADM files with duplicate ID\'s is: ' + nDupFiles);
//        utils.saveJsonSync(duplicateIDs, "duplicateIDs.json");
    }
    esUtils.indexExists('subcircuits', handleEsDB);
};

var processFiles = function (i, start, files) {
    if (i >= files.length) {
        if (stack.length > 0) {
            files = stack.pop();
            start = stack.pop();
            i = stack.pop();
            utils.nextFun(processFiles, i + 1, start, files);
        } else {
            //  This is where we end up when the scan is complete.
            endOfScan();
        }
    } else {
        var fileName = files[i];
        var fullPath = path.join(start, fileName);
        var afterStat = function (err, stat) {
            if (stat.isFile()) {
                if (path.extname(fileName) === '.adm') {
                    utils.readXml(fullPath, function (filename, adm) {
                        addToComponents(filename, adm, i, start, files);
                    });
                } else {
                    utils.nextFun(processFiles, i + 1, start, files);
                }
            } else {
                stack.push(i);
                stack.push(start);
                stack.push(files);
                utils.nextFun(readDir, fullPath);
            }
        };
        fs.stat(fullPath, afterStat);
    }
};

var readDir = function (start) {
    fs.readdir(start, function (err, files) {
        if (err) {
            console.log('Dir \'' + start + '\' not found');
        }
        files = files.sort();
        processFiles(0, start, files);
    });
};

exports.initialize = function (rpath, callback) {
    var outOfDate = true;

    rootPath = path.normalize(rpath);

    if (app.autoRescan) {

        var gitIndex = gitUtils.getGitIndex(rootPath);

        if (gitIndex) {
            gitUtils.watchGitDir(gitIndex, repoChanged);
            console.log('subCircuitScanner is watching path: ' + gitIndex);
        } else {
            console.log('Could not find .git directory. Changes to sub-circuits will not be reflected until restart');
        }

        try {
            outOfDate = fs.statSync(gitIndex).mtime > fs.statSync(savedComponentsFilename).mtime;
        } catch (err) {
            // ignore
        }
    }

    if (!exports.disableCache && fs.existsSync(savedComponentsFilename)) {
        exports.loadComponents();
    }
    console.log('Please wait while the sub-circuit library is scanned.');
    exports.readComponents(rpath, callback);
};

exports.readComponents = function (rpath, callback) {
    if (exports.inScan) {
        return;
    }
    exports.inScan = true;

    esIndex = esUtils.newName('subcircuits');

    var mapping = {
        'mappings': {
            'index_name': {
                'properties': {
                    'index_name': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    }
                }
            },
            'subcircuit': {
                'properties': {
                    'classifications': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    },
                    'name': {
                        'type': 'string',
                        'index': 'not_analyzed'
                    },
                    'position': {
                        'type': 'long'
                    }
                }
            }
        }
    };
    esUtils.post(esIndex, mapping, function (err, resp) {
        if (err !== null) {
            errLog.logError('Error mapping elasticsearch index.', err);
        } else {
            esUtils.createIndex(esIndex, function (err, resp) {
                if (err !== undefined) {
                    errLog.logError('Error creating elasticsearch index.', err);
                } else {
                    whenDoneCallback = callback;
                    stack = [];
                    subcircuits = {};
                    nSubCircuits = 0;
                    subcircuits.idIndex = {};
                    startTime = new Date().getTime();

                    readDir(rootPath);
                }
            });
        }
    });

};

