/**
 * Created by Blake McBride on 5/18/15.
 */


'use strict';


var scScanner = require('./subCircuitScanner');
var path = require('path');
var esUtils = require("../utils/esUtils");

var numComponents = function (x) {
    if (x === null || x === '') {
        return 0;
    }
    return x.split('/').length;
};

var fixLabel = function (lbl) {
    lbl = lbl.replace(/_/g, ' ');
    return lbl.charAt(0).toUpperCase() + lbl.slice(1);
};

var buildComponentTree = function (fromPath, comps, compArray, orgArray, count, path, rootLevels) {
    for (var key in comps) {
        if (key === 'idIndex' || key === '_subcircuits') {
            continue;
        }
        var node = comps[key];
        var newnode = {};
        if (path === '') {
            newnode.id = key;
        } else {
            newnode.id = path + '/' + key;
        }
        newnode.label = fixLabel(key);
        if (node instanceof Array) {  // components
            var cArray;
            if (fromPath === newnode.id) {
                cArray = orgArray;
            } else {
                cArray = [];
            }
            for (var i = 0; i < node.length; i++) {
                var fcomp = node[i];
                var tcomp = {};
                tcomp.id = newnode.id + '/' + fcomp.name;
                tcomp.label = fcomp.name;
                tcomp.symbolUrl = tcomp.id + '/Icon.png';
                tcomp.childCategoriesCount = 0;
                tcomp.childSubcircuitsCount = 0;
                cArray[cArray.length] = tcomp;
            }
            newnode.childCategoriesCount = 0;
            newnode.childSubcircuitsCount = cArray.length;
            if (count > 1) {
                newnode.subClasses = cArray;
            }
        } else {  // category
            var subArray;
            if (newnode.id.indexOf(fromPath) === 0 && rootLevels === numComponents(newnode.id)) {
                subArray = orgArray;
            } else {
                subArray = [];
            }
            if (count > 0) {
                buildComponentTree(fromPath, node, subArray, orgArray, count - 1, newnode.id, rootLevels);
            }
            newnode.childCategoriesCount = subArray.length;
            if (node._subcircuits instanceof Array) {
                newnode.childSubcircuitsCount = node._subcircuits.length;
            } else {
                newnode.childSubcircuitsCount = 0;
            }
            if (count > 1 && subArray.length !== 0 && compArray !== subArray) {
                newnode.subClasses = subArray;
            }
        }
        if (newnode.id.indexOf(fromPath) === 0 && newnode.id.length !== fromPath.length) {
            compArray[compArray.length] = newnode;
        }
    }
};

var buildClassificationTree = function (fromPath, comps, compArray, orgArray, count) {
    var total = 0;
    for (var key in comps) {
        if (key === 'idIndex' || key === '_subcircuits' || key === 'categoryTotal') {
            continue;
        }
        var node = comps[key];
        var newnode = {};
        if (fromPath.length === 0) {
            newnode.id = key;
        } else {
            newnode.id = fromPath + '/' + key;
        }
        newnode.label = fixLabel(key);
        newnode.categoryTotal = node.categoryTotal;
        total += node.categoryTotal;
        var subArray = [];
        if (count > 0) {
            buildClassificationTree(newnode.id, node, subArray, orgArray, count - 1);
        }
        newnode.childCategoriesCount = subArray.length;
        if (node._subcircuits instanceof Array) {
            newnode.childSubcircuitsCount = node._subcircuits.length;
        } else {
            newnode.childSubcircuitsCount = 0;
        }
        if (count > 1 && subArray.length !== 0 && compArray !== subArray) {
            newnode.subClasses = subArray;
        }
        if (newnode.id.indexOf(fromPath) === 0 && newnode.id.length !== fromPath.length) {
            compArray[compArray.length] = newnode;
        }
    }
    return total;
};

exports.isReady = function () {
    return !!scScanner.subcircuits;
};

exports.search = function (classification, searchText, cursor, size, callback) {
    var searchObj = {
        from: cursor,
        size: size,
        query: {
            bool: {
                must: []
            }
        }
    };

    var must = searchObj.query.bool.must;

    if (classification !== '/') {
        must.push({
            match_phrase_prefix: {
                classifications: classification + '/'
            }
        });
    }
    if (searchText !== undefined && searchText !== null && searchText !== '' && searchText !== '_all') {
        must.push({
            match: {
                _all: searchText
            }
        });
    }
    must.push({match: {_type: "subcircuit"}});
    esUtils.get('subcircuits/_search', searchObj, callback);
};

/* fromRoot is actually a relative path through the component tree  */
exports.componentTree = function (fromRoot, count) {
    var comps = scScanner.components;
    var res = {};
    res.classes = [];
    if (fromRoot === null) {
        fromRoot = '';
    } else {
        fromRoot = fromRoot.replace(/!/g, '/');
        if (fromRoot.charAt(0) === '/') {
            fromRoot = fromRoot.substring(1);
        }
    }
    var rootLevels = numComponents(fromRoot);
    count += rootLevels;
    buildComponentTree(fromRoot, comps, res.classes, res.classes, count, '', rootLevels);
    return res;
};

/* fromRoot is actually a relative path through the component tree  */
exports.classificationTree = function (fromRoot, count) {
    var comps = scScanner.subcircuits;
    var res = {};
    res.classes = [];
    fromRoot = fromRoot.replace(/!/g, '/');
    if (fromRoot.charAt(0) === '/') {
        fromRoot = fromRoot.substring(1);
    }
    var rootArray = fromRoot.length === 0 ? [] : fromRoot.split('/');
    for (var i = 0; i < rootArray.length; i++) {
        comps = comps[rootArray[i]];
    }
    res.grandTotal = buildClassificationTree(fromRoot, comps, res.classes, res.classes, count);
    return res;
};

exports.subCircuitList = function (clas, count, cursor) {
    var comps = scScanner.subcircuits;
    var res = {};
    res.subcircuits = [];
    clas = clas.replace(/!/g, '/');
    if (clas.charAt(0) === '/') {
        clas = clas.substring(1);
    }
    var i, n, rootArray = clas.length === 0 ? [] : clas.split('/');
    for (i = 0; i < rootArray.length && comps[rootArray[i]] !== undefined; i++) {
        comps = comps[rootArray[i]];
    }
    comps = comps._subcircuits;
    if (comps instanceof Array) {
        for (i = cursor, n = count; i < comps.length && n > 0; i++, n--) {
            var fcomp = comps[i];
            var tcomp = {};
            tcomp.id = fcomp.id;
            tcomp.name = fcomp.name;
            tcomp.classificationLabels = fcomp.classificationLabels;
            tcomp.position = fcomp.position;  //  in this case, should always be the same as i
            tcomp.description = fcomp.description;
            res.subcircuits.push(tcomp);
        }
    }
    return res;
};

exports.subcircuitsOverview = function (id) {
    var comp = scScanner.subcircuits.idIndex[id];
    var ret = {};
    if (comp !== undefined) {
        ret.id = comp.id;
        ret.name = comp.name;
        ret.classificationLabels = comp.classificationLabels;
        ret.position = comp.position;
        ret.description = comp.description;
        ret.connectors = comp.connectors;
        ret.subcircuitSourceURL = comp.subcircuitSourceURL;
    }
    return ret;
};

exports.subcircuitPath = function (id) {
    var comp = scScanner.subcircuits.idIndex[id];
    if (comp !== undefined) {
        return path.dirname(comp.filename);
    }
    return undefined;
};
