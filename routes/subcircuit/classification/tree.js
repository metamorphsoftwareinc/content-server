/**
 * Created by Blake McBride on 5/19/15.
 */
'use strict';

var express = require('express');
var router = express.Router();
var utils = require('../../../utils/objectUtils');
var app = require('../../../app');

var scInfo = require('../../../modules/subCircuitInfo');


/* The :varx represents arguments separated by slashes
 The ? after the parameter makes the parameter optional
 */
router.get('/:fromRoot?/:count?', function (req, res) {
    var fromRoot = req.params.fromRoot;
    var count = req.params.count;
    var ret;
    if (fromRoot === undefined) {
        fromRoot = '';
    }
    if (count === undefined) {
        count = 2;
    } else {
        count = parseInt(count);
    }
    if (!scInfo.isReady()) {
        res.status(503).send('Service unavailable');
    } else {
        ret = scInfo.classificationTree(fromRoot, count);
        if (utils.deepEquals(ret, {classes: []})) {
            res.status(404).send('Not found');
        } else {
            res.send(ret);
        }
    }
});

module.exports = router;
