/**
 * Created by Blake McBride on 5/22/15.
 */

'use strict';

var express = require('express');
var Q = require('q');
var router = express.Router();
var app = require('../../app');
var sci = require('../../modules/subCircuitInfo');
var objUtils = require('../../utils/objectUtils');

var handleGet = function (req, res) {
    var fromRoot = req.params.fromRoot;
    var search = req.params.search;
    var count = req.params.count;
    var cursor = req.params.cursor;

    /*  Angular is sending the query via req.query
     Node/Mocha is sending it via req.body
     If neither is sending it, either value works
     */

    if (count === null || count === undefined) {
        count = 50;
    } else {
        count = parseInt(count);
    }
    if (cursor === null || cursor === undefined) {
        cursor = 0;
    } else {
        cursor = parseInt(cursor);
        if (cursor < 0) {
            cursor = 0;
        }
    }

    fromRoot = fromRoot ? fromRoot.replace(/!/g, '/') : '/';

    Q.ninvoke(sci, 'search', fromRoot, search, cursor, count)
        .spread(function (searchResult/*, body*/) {

            if (searchResult.body.error) {
                throw Error(searchResult.body.error);
            }

            searchResult = searchResult.body;

            var result = {};

            if (searchResult.hits !== undefined && Array.isArray(searchResult.hits.hits)) {

                searchResult.hits.hits.forEach(function (hit) {

                    if (hit._type === 'index_name') {
                        searchResult.hits.total--;
                        return;
                    }

                    hit._source.searchMetadata = {
                        score: hit._score
                    };
                    hit._source.id = hit._id;
                    var overview = sci.subcircuitsOverview(hit._source.id);
                    if (overview && overview.subcircuitSourceURL) {
                        hit._source.subcircuitSourceURL = overview.subcircuitSourceURL;
                    }

                    result[hit._type] = result[hit._type] || [];
                    result[hit._type].push(hit._source);

                });

            }

            result.max_score = searchResult.hits.max_score;
            result.total = searchResult.hits.total;

            res.send(result);
        })
        .catch(function (err) {
            console.log(new Date().toISOString() + ' /subcircuit/search error: ' + err + ' ' + (err.stack ? err.stack : ''));
            if (res.headersSent === false) {
                res.sendStatus(err.status || 500);
            } else {
                res.close();
            }
        });
};

/* The :varx represents arguments separated by slashes
 The ? after the parameter makes the parameter optional
 */
router.get('/:fromRoot?/:search?/:count?/:cursor?', function (req, res) {
    if (!sci.isReady()) {
        res.status(503).send('Service unavailable');
    } else {
        handleGet(req, res);
    }
});

module.exports = router;

