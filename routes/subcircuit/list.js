/**
 * Created by Blake McBride on 5/20/15.
 */

'use strict';

var express = require('express');
var router = express.Router();
var utils = require('../../utils/objectUtils');
var app = require('../../app');

var scInfo = require('../../modules/subCircuitInfo');

/* The :varx represents arguments separated by slashes
 The ? after the parameter makes the parameter optional
 */
router.get('/:clas?/:count?/:cursor?', function (req, res) {
    var clas = req.params.clas;
    var count = req.params.count;
    var cursor = req.params.cursor;
    var ret;
    if (clas === null || clas === undefined) {
        clas = '';
    }
    if (count === null || count === undefined) {
        count = 50;
    } else {
        count = parseInt(count);
    }
    if (cursor === null || cursor === undefined) {
        cursor = 0;
    } else {
        cursor = parseInt(cursor);
        if (cursor < 0) {
            cursor = 0;
        }
    }
    if (!scInfo.isReady()) {
        res.status(503).send('Service unavailable');
    } else {
        ret = scInfo.subCircuitList(clas, count, cursor);
        res.send(ret);
    }
});

module.exports = router;

