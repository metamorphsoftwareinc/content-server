/**
 * Created by blake on 5/22/15.
 */

'use strict';

var express = require('express');
var router = express();
var path = require('path');
var zipFolder = require('zip-folder');
var archiver = require('archiver');
var ParseDdp = require('parseddp');
var Q = require('q');
var zipUtils = require('../../utils/zipUtils');


var scInfo = require('../../modules/subCircuitInfo');

/* The :varx represents arguments separated by slashes
 The ? after the parameter makes the parameter optional

 type = f = return zip file name
 c = return file contents

 idd = type f = component id
 type c = file name
 */

var getDownloadPath = function () {
    return path.join('public');
};

var zipComponent = function zipComponent(idd, callback) {
    var fixFilename = function (f) {
        return f.replace(/[\.\/\\ %]/g, '_');
    };
    var scPath = scInfo.subcircuitPath(idd);
    if (scPath === undefined) {
        return callback(undefined);
    }
    var zipfilename = fixFilename(path.basename(scPath)) + ".adp";
    var fullZipPath = getDownloadPath() + '/' + zipfilename;
    var exists = waitForUtils.exists(getDownloadPath());
    if (exists) {
        exists = waitForUtils.exists(fullZipPath);
        if (exists) {
            waitForUtils.unlink(fullZipPath);
        }
    } else {
        waitForUtils.mkdir(getDownloadPath(), 511);
    }
    wait.for(zipFolder, scPath, fullZipPath);

    callback(zipfilename);
};

var sendSubcircuitZip = function sendSubcircuitZip(req, res, idd) {
    var options = {
//            root: __dirname + '/' + downloadPath + '/',
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };

    var fullZipPath = getDownloadPath() + '/' + idd;
    var absPath = path.resolve(fullZipPath);
    res.attachment(absPath);
    res.setHeader('Content-Disposition', 'attachment; filename="' + idd + '"');
    res.set('Content-Type', 'application/octet-stream');
    res.sendFile(absPath, options, function (err) {

        if (err) {
            console.log(err);
            res.status(err.status).end();
        } else {
            console.log('Sent:', absPath);
        }
    });
};

var pipeZip = function (srcFolder, output, callback) {
    var zipArchive = archiver('zip');

    output.on('close', function () {
        callback();
    });

    zipArchive.pipe(output);

    zipArchive.bulk([
        {cwd: srcFolder, src: ['**/*'], expand: true}
    ]);

    zipArchive.finalize(function (err, bytes) {
        if (err) {
            callback(err);
        }
    });
};

var getSync = function (req, res) {
    var type = req.params.type;
    var idd = req.params.idd;

    if (type === 'c' || type === 'download') {
        var subcircuitPath = scInfo.subcircuitPath(idd);
        if (subcircuitPath === undefined) {
            return res.status(404).end();
        }
        return zipUtils.sendZip(req, res, subcircuitPath);
    } else if (type === 'info') {
        var subcircuitPath = scInfo.subcircuitPath(idd);
        if (subcircuitPath === undefined) {
            return res.status(404).end();
        }
        var bufs = [],
            zip = archiver('zip');

        zip.bulk([
            {cwd: subcircuitPath, src: ['**/*'], expand: true}
        ]);


        zip.on('data', function (d) {
            bufs.push(d);
        });
        zip.on('end', function () {
            var buf = Buffer.concat(bufs);
            var admParser = new ParseDdp.ParseAdm(idd);

            admParser.getZip = function getZip() {
                return Q(buf);
            };

            admParser.getDatasheetUrl = function getDatasheetUrl(path) {
                return '/rest/external/acminfo/getfile/' + this.id + '/' + encodeURIComponent(path);
            };

            var admInfo = admParser.parse().then(function (json) {
                res.send(json).end();
            }).catch(function (err) {
                console.log(err + ' ' + err.stack);
                res.status(500).end();
                return err;
            });
        });
        zip.on('error', function (err) {
            console.log(err);
            res.status(500).end();
        });
        zip.finalize(function (err) {
            console.log(err);
            res.status(500).end();
        });

    }
};

router.get('/:type/:idd', function (req, res) {
    if (!scInfo.isReady()) {
        res.status(503).send('Service unavailable');
    } else {
        getSync(req, res);
    }
});

module.exports = router;
