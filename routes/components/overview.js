/**
 * Created by Blake McBride on 1/8/15.
 */

'use strict';

var express = require('express');
var router = express.Router();
var utils = require('../../utils/objectUtils');
var app = require('../../app');

var componentInfo = require('../../modules/componentInfo');

/* The :varx represents arguments separated by slashes
 The ? after the parameter makes the parameter optional
 */
router.get('/:idd', function (req, res) {
    var idd = req.params.idd;
    var ret;
    if (!componentInfo.isReady()) {
        res.status(503).send('Service unavailable');
    } else {
        ret = componentInfo.componentsOverview(idd);
        if (utils.deepEquals(ret, {})) {
            res.status(404).send('Not found');
        } else {
            res.send(ret);
        }
    }
});

module.exports = router;

