/**
 * Created by Blake McBride on 2/23/15.
 */

'use strict';

var express = require('express');
var router = express.Router();
var ci = require('../../modules/componentInfo');
var objUtils = require('../../utils/objectUtils');
var Q = require('q');

var handleGet = function (req, res) {
    var fromRoot = req.params.fromRoot;
    var search = req.params.search;
    var count = req.params.count;
    var cursor = req.params.cursor;
    var query;  // the data associated with the REST call
    var columnSearchText;
    var sortColumns;

    /*  Angular is sending the query via req.query
     Node/Mocha is sending it via req.body
     If neither is sending it, either value works
     */
    query = objUtils.countProperties(req.body) === 0 ? req.query : req.body;
    if (query !== undefined) {
        if (query.columnSearchText !== undefined) {
            columnSearchText = JSON.parse(query.columnSearchText);
        }
        if (query.sortColumns !== undefined) {
            sortColumns = [];
            if (Array.isArray(query.sortColumns)) {
                for (var idx in query.sortColumns) {
                    sortColumns.push(JSON.parse(query.sortColumns[idx]));
                }
            } else {
                sortColumns.push(JSON.parse(query.sortColumns));
            }
        }
    }

    if (count === null || count === undefined) {
        count = 50;
    } else {
        count = parseInt(count);
    }
    if (cursor === null || cursor === undefined) {
        cursor = 0;
    } else {
        cursor = parseInt(cursor);
        if (cursor < 0) {
            cursor = 0;
        }
    }

    fromRoot = fromRoot.replace(/!/g, '/');

    Q.ninvoke(ci, 'search', fromRoot, search, cursor, count, columnSearchText, sortColumns)
        .spread(function (searchResult/*, body*/) {

            if (searchResult.body.error) {
                throw Error(searchResult.body.error);
            }

            var body = searchResult.body;

            if (body.hits !== undefined && Array.isArray(body.hits.hits)) {

                body.hits.hits.forEach(function (hit) {

                    body.hits[hit._type] = body.hits[hit._type] || [];

                    hit._source.searchMetadata = {
                        score: hit._score
                    };
                    hit._source.id = hit._id;

                    // Removing connector info from here. We just want them in the overview call.
                    delete hit._source.connectors;

                    body.hits[hit._type].push(hit._source);


                });

                delete body.hits.hits;

            }

            var x = body.hits.component || [];
            var i;
            for (i = 0; i < x.length; i++) {
                if (x[i].datasheet !== undefined) {
                    var b = 0;
                }
            }

            res.send(body.hits);
        })
        .catch(function (err) {
            console.log(new Date().toISOString() + ' /component/search error: ' + err + ' ' + (err.stack ? err.stack : ''));
            if (res.headersSent === false) {
                res.sendStatus(err.status || 500);
            } else {
                res.close();
            }
        });
};

/* The :varx represents arguments separated by slashes
 The ? after the parameter makes the parameter optional
 */
router.get('/:fromRoot?/:search?/:count?/:cursor?', function (req, res) {
    if (!ci.isReady()) {
        res.status(503).send('Service unavailable');
    } else {
        handleGet(req, res);
    }
});

module.exports = router;
