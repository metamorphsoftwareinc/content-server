/**
 * Created by Blake McBride on 2/19/15.
 */

'use strict';

var express = require('express');
var router = express();
var fs = require('fs');
var path = require('path');
var archiver = require('archiver');
var ParseDdp = require('parseddp');
var Q = require('q');
var zipUtils = require('../utils/zipUtils');

var componentInfo = require('../modules/componentInfo');

/* The :varx represents arguments separated by slashes
 The ? after the parameter makes the parameter optional

 type = info     = return zip file name
 download = return file contents

 idd = type info = component id
 type download = file name
 */


var get = function (req, res) {
    var type = req.params.type;
    var idd = req.params.idd;
    var compDir = componentInfo.componentPath(idd);

    if (type === 'download') {
        if (compDir === undefined) {
            return res.status(404).end();
        }
        zipUtils.sendZip(req, res, compDir);
    } else if (type === 'info') {
        if (compDir === undefined) {
            return res.status(404).end();
        }
        var bufs = [],
            zip = archiver('zip');

        zip.bulk([
            {cwd: compDir, src: ['**/*'], expand: true}
        ]);


        zip.on('data', function (d) {
            bufs.push(d);
        });
        zip.on('end', function () {
            var buf = Buffer.concat(bufs);
            var acmParser = new ParseDdp.ParseAcm(idd);


            acmParser.getZip = function getZip() {
                return Q(buf);
            };

            acmParser.getDatasheetUrl = function getDatasheetUrl(path) {
                return '/rest/external/acminfo/getfile/' + this.id + '/' + encodeURIComponent(path);
            };

            var acmInfo = acmParser.parse().then(function (json) {
                res.send(json).end();
            }).catch(function (err) {
                console.log(err + ' ' + err.stack);
                res.status(500).end();
                return err;
            });
        });
        zip.on('error', function (err) {
            console.log(err);
            res.status(500).end();
        });
        zip.finalize(function (err) {
            console.log(err);
            res.status(500).end();
        });
    } else if (type === 'acm') {
        var acmPath = componentInfo.acmPath(idd);
        if (acmPath === undefined) {
            return res.status(404).end();
        }
        res.sendFile(path.normalize(acmPath));
    } else {
        res.status(404).end();
    }
};

router.get('/:type/:idd', function (req, res) {
    if (!componentInfo.isReady()) {
        res.status(503).send('Service unavailable');
    } else {
        try {
            get(req, res);
        } catch (e) {
            console.log(e + ' ' + e.stack);
        }
    }
});

module.exports = router;
