var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
    res.header('Pragma', 'no-cache');
    res.redirect('/componentBrowser/');
});

module.exports = router;
