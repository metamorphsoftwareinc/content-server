var fs = require('fs');
var path = require('path');

exports.getGitIndex = function (rootPath) {
    var gitPath = path.normalize(rootPath);

    while (path.dirname(gitPath) !== gitPath) {
        try {
            var stat = fs.statSync(path.join(gitPath, '.git'));
            if (stat.isDirectory()) {
                break;
            }
        } catch (e) {
        }
        gitPath = path.dirname(gitPath);
    }

    var gitIndex = path.normalize(path.join(gitPath, '.git', 'index'));

    if (fs.existsSync(gitIndex)) {
        return gitIndex;
    } else {
        return undefined;
    }
};

exports.watchGitDir = function (index, changedCallback) {
    var indexStat = fs.statSync(index),
        timeoutID,
        watchID;

    watchID = fs.watch(path.dirname(index), function () {
        var newStat = fs.statSync(index);
        var changed = newStat.mtime.getTime() !== indexStat.mtime.getTime() || newStat.ino !== indexStat.ino;
        indexStat = newStat;
        if (changed) {
            if (timeoutID) {
                clearTimeout(timeoutID);
            }
            // it seems we get a notification for each write(1). So wait some time for the write to be complete
            timeoutID = setTimeout(function () {
                timeoutID = undefined;
                changedCallback();
            }, 5000);
        }
    });
};
