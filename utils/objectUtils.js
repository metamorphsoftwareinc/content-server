/**
 * Created by Blake McBride on 1/15/15.
 */

'use strict';

var fs = require('fs');
var xml2js = require('xml2js');
var util = require('util');

JSON.minify = JSON.minify || require("node-json-minify");  // enables comments in JSON files


exports.deepEquals = function (x, y) {
    // if both x and y are null or undefined and exactly the same
    if (x === y) {
        return true;
    }

    // if they are not strictly equal, they both need to be Objects
    if (!( x instanceof Object ) || !( y instanceof Object )) {
        return false;
    }

    // they must have the exact same prototype chain, the closest we can do is
    // test there constructor.
    if (x.constructor !== y.constructor) {
        return false;
    }

    for (var p in x) {
        // other properties were tested using x.constructor === y.constructor
        if (!x.hasOwnProperty(p)) {
            continue;
        }

        // allows to compare x[ p ] and y[ p ] when set to undefined
        if (!y.hasOwnProperty(p)) {
            return false;
        }

        // if they have the same strict value or identity then they are equal
        if (x[p] === y[p]) {
            continue;
        }

        // Numbers, Strings, Functions, Booleans must be strictly equal
        if (typeof( x[p] ) !== "object") {
            return false;
        }

        // Objects and Arrays must be tested recursively
        if (!exports.deepEquals(x[p], y[p])) {
            return false;
        }
    }

    for (p in y) {
        // allows x[ p ] to be set to undefined
        if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) {
            return false;
        }
    }
    return true;
};

exports.saveJson = function (json, filename, callback) {
    callback = callback || function () {
        };
    fs.writeFile(filename, JSON.stringify(json, null, 4), function (err) {
        if (err) {
            console.log(err);
        }
        callback(err);
    });
};

exports.saveJsonSync = function (json, filename) {
    fs.writeFileSync(filename, JSON.stringify(json, null, 4));
};

exports.loadJson = function (filename) {
    var jsonStr = JSON.minify(fs.readFileSync(filename, {encoding: 'ascii'}));
    return JSON.parse(jsonStr);
};

/**
 * Async XML parser that passes an equivalent JSON object to it's callback function.
 *
 * @param filename
 * @param callback(filename, result)
 */
exports.readXml = function (filename, callback) {
    var xmlParser = new xml2js.Parser();
    fs.readFile(filename, function (err, data) {
        xmlParser.parseString(data, function (err, result) {
            callback(filename, result);
        });
    });
};

exports.readXmlSync = function (filename) {
    var res;
    var xmlParser = new xml2js.Parser();
    xmlParser.parseString(fs.readFileSync(filename), function (err, result) {
        res = result;
    });
    return res;
};

exports.printJson = function (j) {
    util.print(JSON.stringify(j, null, 4));
};

/**
 * Calls fun asynchronously with the arguments (args) passed.
 * This is done without increasing the stack use because the caller can end.
 *
 * @param fun
 * @param args...
 */
exports.nextFun = function (fun, args) {
    if (fun === undefined) {
        return;
    }
    var vargs = arguments;
    delete vargs['0'];
    var array = Object.keys(vargs).map(function (k) {
        return vargs[k];
    });

    setImmediate(function () {
        fun.apply(fun, array);
    });
    //process.nextTick(function() {
    //    fun.apply(fun, array);
    //});
};

/**
 * Returns the number of key/value pairs at the top level.
 * Returns 0 if not an object.
 *
 * @param obj
 * @returns number of properties in the object
 */
exports.countProperties = function (obj) {
    var count;
    if (obj === null || typeof obj !== 'object') {
        return 0;
    }
    /*
     count = 0;
     for(var prop in obj) {
     if (obj.hasOwnProperty(prop)) {
     ++count;
     }
     }
     */
    count = Object.keys(obj).length;
    return count;
}

/**
 * Is string obj parsable as a number?
 *
 * @param obj
 * @returns {boolean}
 */
exports.isNumeric = function (obj) {
    obj = typeof(obj) === 'string' ? obj.replace(/,/g, '') : obj;
    return !isNaN(parseFloat(obj)) && isFinite(obj) && Object.prototype.toString.call(obj).toLowerCase() !== "[object array]";
};

/**
 * Parse string obj into a number
 *
 * @param obj
 * @returns {Number}
 */
exports.toNumber = function (obj) {
    obj = typeof(obj) === 'string' ? obj.replace(/,/g, '') : obj;
    return parseFloat(obj);
};

/**
 * Returns a key in an object.  The object is presumed to only have one key which will be returned.
 * If the object has more than one key, an arbitrary key is returned.
 *
 * @param obj
 * @returns a key in obj
 */
exports.getKey = function (obj) {
    var res, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            res = key;
            break;
        }
    }
    return res;
};
