'use strict';
var fs = require('fs');

var stream = require('stream');
var Transform = stream.Transform ||
    require('readable-stream').Transform;

var schemaVersion = 'schema2';


var LineReader = function LineReader(options) {
    // allow use without new
    if (!(this instanceof LineReader)) {
        return new LineReader(options);
    }

    options = options || {};
    options.objectMode = true;
    Transform.call(this, options);
};
require('util').inherits(LineReader, Transform);

LineReader.prototype._transform = function (chunk, encoding, done) {
    var data = chunk.toString();
    if (this._lastLineData) data = this._lastLineData + data;

    var lines = data.split('\n');
    this._lastLineData = lines.splice(lines.length - 1, 1)[0];

    lines.forEach(this.push.bind(this));
    done();
};

LineReader.prototype._flush = function (done) {
    if (this._lastLineData) this.push(this._lastLineData);
    this._lastLineData = null;
    done();
};

module.exports.LineReader = LineReader;

exports.loadCacheFile = function (filename, name, callback) {
    try {
        fs.statSync(filename);
    } catch (e) {
        callback();
    }
    var inputStream = fs.createReadStream(filename, {encoding: 'utf8'});
    var lineInput = new LineReader();
    var idIndex = {};
    inputStream.pipe(lineInput);
    var fileSchemaVersion;
    lineInput.on('readable', function () {
        var line;
        while (line = lineInput.read()) {
            if (fileSchemaVersion === undefined) {
                fileSchemaVersion = line;
            } else {
                if (fileSchemaVersion === schemaVersion) {
                    var o = JSON.parse(line);
                    idIndex[o.id] = o;
                    // TODO: check o.filename starts with directory containing .git dir
                }
            }
        }
    });
    lineInput.on('end', function () {
        var components = {idIndex: idIndex};
        for (var id in idIndex) {
            var category = components;
            category.categoryTotal = (category.categoryTotal || 0) + 1;
            idIndex[id].classificationLabels.forEach(function (classification) {
                if (category[classification] === undefined) {
                    category[classification] = {};
                    category[classification][name] = [];
                }
                category = category[classification];
                category.categoryTotal = (category.categoryTotal || 0) + 1;
            });
            category[name].push(idIndex[id]);
        }

        callback(components.categoryTotal ? components : null);
    });
};

exports.writeCacheFile = function (idIndex, filename, name, callback) {
    var outputStream = fs.createWriteStream(filename, {encoding: 'utf8'});
    outputStream.write(schemaVersion + '\n');
    var keys = Object.getOwnPropertyNames(idIndex);
    var i = 0;
    write();
    function write() {
        var ok = true;
        while (i < keys.length && ok) {
            outputStream.write(JSON.stringify(idIndex[keys[i]]));
            ok = outputStream.write('\n');
            i++;
        }
        if (i < keys.length) {
            outputStream.once('drain', write);
        } else {
            outputStream.end(callback);
        }
    }
};
