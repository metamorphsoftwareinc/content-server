'use strict';

var fs = require('fs');
var archiver = require('archiver');
var path = require('path');
var contentDisposition = require('content-disposition');

var pipeZip = function pipeZip(srcFolder, output, callback) {
    var zipArchive = archiver('zip', {store: true});

    output.on('close', function () {
        callback();
    });

    zipArchive.pipe(output);

    zipArchive.bulk([
        {cwd: srcFolder, src: ['**/*'], expand: true}
    ]);

    zipArchive.finalize(function (err/*, bytes*/) {
        if (err) {
            callback(err);
        }
    });
};

function readdir(srcFolder, callback, done) {
    fs.readdir(srcFolder, function (err, files) {
        if (err) {
            return done(err);
        }

        var pending = files.length;
        if (!pending) {
            return done(null);
        }

        files.forEach(function (file) {
            fs.lstat(path.join(srcFolder, file), function (err, stats) {
                if (err) {
                    return done(err);
                }

                if (stats.isDirectory()) {
                    // callback(path.join(srcFolder, file), stats)
                    files = readdir(path.join(srcFolder, file), callback, function (err) {
                        if (err) {
                            return done(err);
                        }

                        pending -= 1;
                        if (!pending) {
                            done(null);
                        }
                    });
                } else {
                    callback(path.join(srcFolder, file), stats);
                    pending -= 1;
                    if (!pending) {
                        done(null);
                    }
                }
            });
        });
    });
}

var sendFiles = function (srcFolder, output, callback) {

    readdir(srcFolder, function (file, stats) {
        if (stats.isFile()) {
            output.write('- ' + stats.size + ' ');
            output.write(encodeURI(path.resolve(file)) + ' ');
            output.write(path.relative(srcFolder, path.resolve(file)) + '\r\n');
        }

    }, function (err) {
        if (err) {
            callback(err);
        } else {
            output.end();
            callback(null);
        }
    });
};

exports.sendZip = function sendZip(req, res, compDir) {
    res.setHeader('Content-Disposition', contentDisposition(path.basename(compDir) + '.zip'));
    res.set('Content-Type', 'application/octet-stream');

    if (req.headers['x-mod-zip']) {
        // http://wiki.nginx.org/NgxZip
        res.set('X-Archive-Files', 'zip');
        return sendFiles(compDir, res, function (err) {
            if (err) {
                if (!err.headersSent) {
                    return res.status(500).end();
                }
                return res.close();
            }
        });
    }

    return pipeZip(compDir, res, function (err) {
        if (err) {
            if (!err.headersSent) {
                return res.status(500).end();
            }
            return res.close();
        }
    });
};
