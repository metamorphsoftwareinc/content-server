/**
 * Created by Blake McBride on 6/26/15.
 */


'use strict';


exports.convertToEngineering = function (text) {
    var num, pow;
    var find = function (regex, str) {
        var r = regex.exec(str);
        if (r == null || r.length === 0) {
            return null;
        }
        return r[0];
    };
    var found = find(/^-?[0-9]*\.?[0-9]+[eE]-?[0-9]+/, text);
    if (found === null) {
        found = find(/^-?[0-9]*\.?[0-9]+(?![\d\.])(?!["])/, text);
        if (found === null) {
            return text;
        }
        num = Number(found);
        pow = 0;
    } else {
        num = Number(find(/-?[0-9]*\.?[0-9]+[eE]/, found).slice(0, -1));
        pow = Number(find(/[eE]-?[0-9]+/, found).substring(1));
    }
    if (num === 0) {
        return text.replace(found, '0.0');
    }
    while (num < 1 && num > -1) {
        num *= 10;
        pow--;
    }
    while (num >= 10 || num <= -10) {
        num /= 10;
        pow++;
    }
    while (pow % 3 !== 0) {
        num *= 10;
        pow--;
    }
    if (pow > 24 || pow < -24) {
        return text;
    }
    num = Math.round(num * 1000) / 1000;
    var letr = 'yzafpnum kMGTPEZY'[pow / 3 + 8];
    num = num + '';
    if (num.length === 1) {
        num += '.0';
    }
    return text.replace(found, num + (letr === ' ' ? '' : letr));
};

