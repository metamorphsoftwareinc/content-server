/**
 * Created by Blake McBride on 5/26/15.
 */

'use strict';

exports.logError = function (msg, err) {
    console.log('-----');
    console.log(new Date());
    console.log(msg);
    if (err !== undefined && err !== null && err !== '') {
        console.log(err);
    }
    console.log('-----');
};
