/**
 * Created by Blake McBride on 1/23/15.
 */

//  elasticsearch utils


'use strict';


var elasticsearch = require('elasticsearch');
var request = require('request');
var errLog = require('./errorReporter');

var client;
var esHost;

var check = function () {
    if (client !== undefined) {
        client.ping({
            requestTimeout: 1000,
            // undocumented params are appended to the query string
            hello: "elasticsearch!"
        }, function (error) {
            if (error) {
                errLog.logError('elasticsearch error', error);
                client.close();
                client = undefined;
            } else {
                //               console.log('All is well');
            }
        });
    }
};

exports.connect = function (host) {
    if (client === undefined) {
        if (host === undefined) {
            host = 'localhost:9200';
        }
        esHost = host;
        client = new elasticsearch.Client({
            host: host,
            apiVersion: '1.4'
        });
        check();
    }
};

/**
 * Add a new document.  Error produced if document already existed.
 *
 * @param index
 * @param type
 * @param id
 * @param doc
 * @param callback(error, response)
 */
exports.addDoc = function (index, type, id, doc, callback) {
    if (client !== undefined) {
        var json = {
            index: index,
            type: type,
            id: id,
            body: doc
        };
        client.create(json, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

/**
 * Read a document.
 *
 * Note that it takes a little time for elastic to fully register a change.  Therefore, updating a document
 * and then immediately reading may return the old document (presumable unless you proceed through the callback).
 *
 * @param index
 * @param type
 * @param id
 * @param callback
 */
exports.getDoc = function (index, type, id, callback) {
    if (client !== undefined) {
        var data = {
            index: index,
            type: type,
            id: id
        };
        client.get(data, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

/**
 * Delete a specific document.
 *
 * @param index
 * @param type
 * @param id
 * @param callback
 */
exports.deleteDoc = function (index, type, id, callback) {
    if (client !== undefined) {
        var data = {
            index: index,
            type: type,
            id: id
        };
        client.delete(data, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

/**
 * Find all documents that match a query.
 *
 * response.found = true/false
 * response._source has documents
 *
 * @param index
 * @param query
 * @param callback(error, response)
 */
exports.findDocs = function (index, query, count, cursor, callback) {
    if (client !== undefined) {
        var data = {
            index: index,
            from: cursor,
            size: count,
            q: query
        };
        client.search(data, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

/**
 * Find all documents that match a query within a category.
 *
 * response.found = true/false
 * response._source has documents
 *
 * @param index
 * @param category
 * @param query
 * @param callback(error, response)
 */
exports.findDocsInCategory = function (index, category, query, count, cursor, callback) {
    if (client !== undefined) {
        var data = {
            index: index,
            from: cursor,
            size: count,
//            analyze_wildcard: true,
            q: 'classifications:' + category + '\* AND ' + query
        };
        client.search(data, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

/**
 * Delete an index.
 *
 * @param index
 * @param callback(error. response)
 */
exports.deleteIndex = function (index, callback) {
    if (client !== undefined) {
        var data = {
            index: index
        };
        client.indices.delete(data, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

/**
 * Does document exist?
 *
 * @param index
 * @param type
 * @param id
 * @param callback(error, exists)   exists = true/false
 */
exports.docExists = function (index, type, id, callback) {
    if (client !== undefined) {
        var json = {
            index: index,
            type: type,
            id: id
        };
        client.exists(json, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

/**
 * Replace an existing document or add a new one if it isn't already present.
 *
 * @param index
 * @param type
 * @param id
 * @param doc
 * @param callback(error, response) - same as for addRecord
 */
exports.updateDoc = function (index, type, id, doc, callback) {
    if (client !== undefined) {
        var json = {
            index: index,
            type: type,
            id: id
        };
        client.exists(json, function (err, exists) {
            if (err === undefined) {
                if (exists) {
                    var dd = {
                        index: index,
                        type: type,
                        id: id
                    };
                    client.delete(dd, function (err, res) {
                        if (err === undefined) {
                            exports.addDoc(index, type, id, doc, callback);
                        } else {
                            console.error(err);
                        }
                    });
                } else {
                    exports.addDoc(index, type, id, doc, callback);
                }
            } else {
                console.error(err);
            }
        });
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

/**
 * Does a particular index (or alias) exist?
 *
 * response = true / false
 *
 * @param index
 * @param callback(error. response)
 */
exports.indexExists = function (index, callback) {
    if (client !== undefined) {
        var data = {
            index: index
        };
        client.indices.exists(data, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

exports.newName = function (index) {
    return index + '-' + (new Date()).getTime();
};

/**
 * Post json data directly to elasticsearch.
 * Will create index if it doesn't exist.
 *
 * @param path
 * @param json
 * @param callback
 */
exports.post = function (path, json, callback) {
    if (client !== undefined) {
        var params = {
            method: 'POST',
            url: 'http://' + esHost + '/' + path,
            json: true,
            body: json
        };
        request(params, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

/**
 * Get json data directly to elasticsearch.
 * Will create index if it doesn't exist.
 *
 * @param path
 * @param json
 * @param callback
 */
exports.get = function (path, json, callback) {
    if (client !== undefined) {
        var params = {
            method: 'GET',
            url: 'http://' + esHost + '/' + path,
            json: true,
            body: json
        };
        request(params, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

/**
 * Keep track of the unaliased / real name when starting to use a new index.
 *
 * @param index
 * @param callback
 */
exports.createIndex = function (index, callback) {
    exports.addDoc(index, 'index_name', 'name', {'index_name': index}, callback);
};

/**
 * Get the unaliased index name.
 *
 * @param index - the alias name
 * @param callback(err, resp) - resp = the name or undefined
 */
exports.getIndexName = function (index, callback) {
    exports.getDoc(index, 'index_name', 'name', function (err, resp) {
        if (err === undefined && resp.found === true) {
            callback(undefined, resp._source.index_name);
        } else {
            callback(err, undefined);
        }
    });
};

exports.createAlias = function (index, alias, callback) {
    if (client !== undefined) {
        var data = {
            index: index,
            name: alias
        };
        client.indices.putAlias(data, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

exports.deleteAlias = function (index, alias, callback) {
    if (client !== undefined) {
        var data = {
            index: index,
            name: alias
        };
        client.indices.deleteAlias(data, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

/**
 * Switch an alias from pointing to one index to point to another atomically.
 *
 * @param oldIndex
 * @param newIndex
 * @param alias
 * @param callback
 */
exports.changeAlias = function (oldIndex, newIndex, alias, callback) {
    if (client !== undefined) {
        var data = {
            body: {
                actions: [
                    {remove: {index: oldIndex, alias: alias}},
                    {add: {index: newIndex, alias: alias}}
                ]
            }
        };
        client.indices.updateAliases(data, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

/**
 * Get a list of all of the indexes.  Does not include aliases.
 *
 * @param callback
 */
exports.getAllIndexes = function (callback) {
    if (client !== undefined) {
        var data = {
            h: 'index'
        };
        client.cat.indices(data, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};

exports.getAllAliases = function (callback) {
    if (client !== undefined) {
        var data = {
            h: 'alias'
        };
        client.cat.aliases(data, callback);
    } else if (callback !== undefined) {
        callback('elastic search not connected', undefined);
    }
};
