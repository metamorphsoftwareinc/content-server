/**
 * Created by Blake McBride on 3/10/15.
 */

'use strict';

var wait = require("wait.for");
var fs = require('fs');

/*
 The functions in this module are needed to translate "non-standard" callback signatures
 into "standard" callback signatures that wait.for expects.
 Standard callback take (err, data)
 err is either null or an Error object
 */

exports.exists = function (path) {
    var fun = function (path, stdcallback) {
        fs.exists(path, function (exists) {    // what it really returns
            return stdcallback(null, exists);  // make it standard
        });
    };
    return wait.for(fun, path);
};

exports.unlink = function (path) {
    var fun = function (path, stdcallback) {
        fs.unlink(path, function () {
            return stdcallback(null, null);
        });
    };
    return wait.for(fun, path);
};

exports.mkdir = function (path, mode) {
    var fun1 = function (path, stdcallback) {
        fs.mkdir(path, function () {     // what it really returns
            return stdcallback(null, null);  // make it standard
        });
    };
    var fun2 = function (path, mode, stdcallback) {
        fs.mkdir(path, mode, function () {     // what it really returns
            return stdcallback(null, null);  // make it standard
        });
    };
    if (mode === undefined) {
        return wait.for(fun1, path);
    }
    return wait.for(fun2, path, mode);
};
