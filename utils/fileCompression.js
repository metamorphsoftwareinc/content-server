/**
 * Created by Blake McBride on 2/20/15.
 */

'use strict';

var AdmZip = require('adm-zip');

exports.zipDirToFile = function (zipfilename, dir) {
    var zip = new AdmZip();
    zip.addLocalFolder(dir);
    zip.writeZip(zipfilename);
};

