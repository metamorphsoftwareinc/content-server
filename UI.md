Component Server Web Application UI Design Specification
========================================================

The Component Server Web Application is a browser application allowing
users to query and search through the MetaMorph component database in
order to find and download their desired components.  The front-end is
written in HTML utilizing the AngularJS tool-set.  The back-end is our
currently existing Component Server.  The two communicate through
standard REST services.  The indented audience is the general public.

User Needs
----------

User need were identified as follows.  *Items marked with an asterisk
are targeted for the first pass of the project.*

1. Browse categories - the user will need the ability to browse
through the hierarchy of component categories in order to find the
component they seek.  This will be only one method of traversing the
available components.*

2. List and View components within a category*

3. Filter & sort based on available fields

4. Full text search both overall and within a category*

5. Ability to download component packages

6. Inspect a component's meta-data

7. Paging - the user will need the ability to page through large
result sets rather than seeing everything on a long page.*

UI Design
---------

The application will be displayed on a single web page that will change
appearance based on the context.  It shall look as follows.

![UI Picture goes here](UI.png)

* S - Search bar
* C - Component hierarchy displayed as a tree.

>As the user descends into the component category hierarchy, the tree
>will display a relative position rather than always showing from
>the root.

* M - Main display area

>The main display area will include two views.  The first is a table view
>in which each component is on a row, and each column represents a property.
>The second view is an item list style in which each component is represented
>in a rectangle consisting of multiple lines describing the component.

* V - View mode selection

>This will allow a user to select the desired view mode.

* E - Elimination area

>This area allows a user to reduce the number of
>components by creating restrictions based on common component properties.

* P - Paging controls

Implementation Notes
--------------------

1. Use AngularJS services to interact with the server

2. Correct repo setup

3. Technologies: gulp, SCSS, ISI\_ui\_components, BootStrap, Styling,
Material Design

REST API Design
---------------
