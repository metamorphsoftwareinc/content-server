/* globals it, describe, before */

var assert = require('assert');

var numericUtils = require('../utils/numericUtils');

describe('numericUtils', function() {

    it('convert to engineering', function () {
        var input = '0.024" L x 0.012" W (0.60mm x 0.30mm)';
        var converted = numericUtils.convertToEngineering(input);

        assert.notEqual(converted.substr(0, 3), 'NaN');
        assert.equal(converted, input);
    });

    [
        ['0.000000001m', '1.0nm'],
        ['1e9', '1.0G'],
        ['4.7e-06', '4.7u'],
        ['.180', '180m'],
        ['1.80e-1', '180m'],
        ['1.80', '1.8'],
        ['1.8e0', '1.8'],
        ['18.0', '18'],
        ['1.80e1', '18'],
        ['180.0', '180'],
        ['1.80e02', '180'],
        ['1800.0', '1.8k'],
        ['1.8e3m', '1.8km'],
        ['0.00001"', '0.00001"'], // don't output 10u"
    ].forEach(function (input, i) {
        it('convert to engineering ' + i, function () {
            var converted = numericUtils.convertToEngineering(input[0]);

            assert.equal(converted, input[1]);
        });
    });

});
