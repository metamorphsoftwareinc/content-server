/**
 * Created by Blake McBride on 1/14/15.
 *
 */

/* globals it, describe, before */

'use strict';


var assert = require("assert");
var fs = require('fs');
var utils = require('../utils/objectUtils');
var should = require('should');
var http = require('http');

var count = 0;


//it('Scan GIT repository', function (done) {
//    this.timeout(60000);  // give test server up to 1 minutes
//    console.warn('Scan GIT repository count = ' + (++count));
//    gitScanner.readComponents('mocha-tests/test-components', function (comps) {
//        var comps2 = gitScanner.loadComponents('mocha-tests/test-components.json');
//        assert.deepEqual(comps, comps2, 'GIT scanner error');
//        done();
//    });
//});
//
//
//it('Save / load Json components', function (done) {
//    console.warn('Save / load Json components count = ' + (++count));
//    var testFile = 'mocha-tests/test-save.json';
//    var comps = gitScanner.loadComponents('mocha-tests/test-components.json');
//    utils.saveJsonSync(comps, testFile);
//    var comps2 = gitScanner.loadComponents(testFile);
//    assert.deepEqual(comps, comps2, 'save/load error');
//    fs.unlink(testFile);
//    done();
//});


var errorMessage = function (err, res) {
    if (err !== null) {
        return err.message;
    }
    return res.error.message;
};

var hostname = 'localhost';
var port = 3000;

var createTests = false;

var testES = function () {
    it('Test elasticsearch availability', function(done) {
        var options = {
            hostname: 'localhost',
            port: 9200,
            method: 'GET'
        };
        var req = http.request(options, function (res) {
            done();
        });
        req.on('error', function (e) {
            assert.equal(false, true, 'Error accessing elasticsearch.  It is up?');
            done();
        });
        req.end();
    });
};

var RESTGet = function (service, args, inJson, callback) {
    var userString;
    var options = {
        hostname: hostname,
        port: port,
        method: 'GET',
        path: service + args
    };
    if (inJson !== undefined  &&  inJson !== null) {
        userString = JSON.stringify(inJson);
        var headers = {
            'Content-Type': 'application/json',
            'Content-Length': userString.length
        };
        options.headers = headers;
    }
    var req = http.request(options, function (res) {
        if (res.statusCode === 200) {
            var responseString = '';
            res.on('data', function (data) {
                responseString += data;
            });
            res.on('end', function () {
                callback(responseString);
            });
        } else {
            return callback(new Error(res.statusMessage));
        }
    });
    req.on('error', function (e) {
        assert.equal(false, true, e.message);
    });
    if (userString !== undefined) {
        req.write(userString);
    }
    req.end();

};

var restTest = function (testNum, service, args, inJson, sorter) {
    if (!sorter && service.indexOf('/classification/tree') === 0) {
        sorter = classificationsSorter;
    }
    if (!sorter && service.indexOf('/components/search') === 0) {
        sorter = function (response) {
            response.component.sort(idCompare);
        };
    }
    sorter = sorter || function () {};
    it('Test ' + testNum + ' - verify ' + service, function(done) {
        RESTGet(service, args, inJson, function (res) {
            if (res instanceof Error) {
                done(res);
            }
            var responseObject = JSON.parse(res);
            sorter(responseObject);
            var file = 'mocha-tests/results/result-' + testNum + '.json';
            if (createTests) {
                utils.saveJson(responseObject, file, done);
            } else {
                var expected = utils.loadJson(file);
                assert.deepEqual(responseObject, expected);
                done();
            }
        });
    });
};

function idCompare(a, b) {
    return a.id.localeCompare(b.id);
}

function classificationsSorter(response) {
    var sortSubclasses = function (classes) {
        if (!classes) {
            return;
        }
        classes.sort(idCompare);
        classes.forEach(function (class_) {
            sortSubclasses(class_.subClasses);
        });
    };
    response.classes.sort(idCompare);
    response.classes.forEach(function (class_) {
        sortSubclasses(class_.subClasses)
    });
}

describe('Component Tests', function() {
   describe('REST tests', function() {
       testES();
       restTest('01', '/classification/tree', '');
       restTest('02', '/components/list', '/passive_components!resistors!single_components/4');
       restTest('03', '/components/overview', '/1a324a97-e3ca-490a-998d-8772ff931866');
       restTest('04', '/classification/tree', '/!/1');
       restTest('05', '/classification/tree', '/!/2');
       restTest('06', '/classification/tree', '/!/3');
       restTest('07', '/classification/tree', '/!/4');
       restTest('08', '/classification/tree', '/passive_components/2');
       restTest('09', '/classification/tree', '/passive_components!resistors/2');
       restTest('10', '/components/list', '/passive_components!resistors!single_components/4/0');
       restTest('11', '/components/list', '/passive_components!resistors!single_components/4/2');
       restTest('12', '/components/search', '/!/TMK325BJ475MNHT');
       restTest('13', '/components/search', '/!/0.01');
       restTest('14', '/components/search', '/!', {
           Resistance: 1000000
       });
       restTest('15', '/components/search', '/!', {
           Resistance: 1000,
           'Octopart Mpn': 'ERJ'
       });
       restTest('16', '/components/search', '/!', {
           Resistance: '100-400'
       });
       restTest('17', '/components/search', '/!', {
           Resistance: '<=100'
       });
       it('Test paginated search results are complete', function(done) {
           RESTGet('/components/search', '/!/_all/286/0', null, function (res) {
               assert.equal(JSON.parse(res).component.length, 286);
               done();
           });
       });
    });
});
