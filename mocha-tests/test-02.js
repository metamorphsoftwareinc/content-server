/**
 * Created by Blake McBride on 5/26/15.
 */

/* globals it, describe, before */

'use strict';


var assert = require("assert");
var fs = require('fs');
var utils = require('../utils/objectUtils');
var should = require('should');
var http = require('http');

var count = 0;

var errorMessage = function (err, res) {
    if (err !== null) {
        return err.message;
    }
    return res.error.message;
};

var hostname = 'localhost';
var port = 3000;

var createTests = false;

var testES = function () {
    it('Test elasticsearch availability', function(done) {
        var options = {
            hostname: 'localhost',
            port: 9200,
            method: 'GET'
        };
        var req = http.request(options, function (res) {
            done();
        });
        req.on('error', function (e) {
            assert.equal(false, true, 'Error accessing elasticsearch.  It is up?');
            done();
        });
        req.end();
    });
};

var restTest = function (testNum, service, args, inJson) {
    it('Test ' + testNum + ' - verify ' + service, function(done) {
        var userString;
        var options = {
            hostname: hostname,
            port: port,
            method: 'GET',
            path: service + args
        };
        if (inJson !== undefined  &&  inJson !== null) {
            userString = JSON.stringify(inJson);
            var headers = {
                'Content-Type': 'application/json',
                'Content-Length': userString.length
            };
            options.headers = headers;
        }
        var req = http.request(options, function (res) {
            if (res.statusCode === 200) {
                var file = 'mocha-tests/scresult-' + testNum + '.json';
                var responseString = '';
                res.on('data', function (data) {
                    responseString += data;
                });
                res.on('end', function () {
                    var responseObject = JSON.parse(responseString);
                    if (createTests) {
                        utils.saveJson(responseObject, file, done);
                    } else {
                        var expected = utils.loadJson(file);
                        assert.deepEqual(responseObject, expected);
                        done();
                    }
                });
            } else {
                assert.equal(false, true, res.statusMessage);
            }
        });
        req.on('error', function (e) {
            assert.equal(false, true, e.message);
            done();
        });
        if (userString !== undefined) {
            req.write(userString);
        }
        req.end();
    });
};


describe('Sub-circuit Tests', function() {
    describe('REST tests', function() {
        testES();
        restTest('01', '/subcircuit/classification/tree', '');
        restTest('02', '/subcircuit/list', '/Unclassified');
        restTest('03', '/subcircuit/overview', '/d6bba21a-0842-486b-8b62-6c27f2cb451e');
        restTest('04', '/subcircuit/search', '/!/Sensor');
    });
});


