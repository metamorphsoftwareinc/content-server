

'use strict';


var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs-extra');

var routes = require('./routes/index');
var users = require('./routes/users');

var gitScanner = require('./modules/gitScanner');
var subCircuitScanner = require('./modules/subCircuitScanner');

var classificationTree = require('./routes/classification/tree');
var componentsOverview = require('./routes/components/overview');
var componentsList = require('./routes/components/list');
var componentsSearch = require('./routes/components/search');
var getComponent = require('./routes/getcomponent');

var scTree = require('./routes/subcircuit/classification/tree');
var scList = require('./routes/subcircuit/list');
var scOverview = require('./routes/subcircuit/overview');
var scSearch = require('./routes/subcircuit/search');
var scGet = require('./routes/subcircuit/getsubcircuit');

var esUtils = require('./utils/esUtils');
var objUtils = require('./utils/objectUtils');


var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// should we allow cross-site scripting?
if (true) {
    app.all('*', function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        next();
    });
}

app.use('/', routes);
app.use('/users', users);
app.use('/classification/tree', classificationTree);
app.use('/components/overview', componentsOverview);
app.use('/components/list', componentsList);
app.use('/components/search', componentsSearch);
app.use('/getcomponent', getComponent);

app.use('/subcircuit/classification/tree', scTree);
app.use('/subcircuit/list', scList);
app.use('/subcircuit/overview', scOverview);
app.use('/subcircuit/search', scSearch);
app.use('/subcircuit/getsubcircuit', scGet);

// exporting a function from app
//exports.getAppDir = function () {
//    return __dirname;
//};

var parseConfig = function () {

    var config;

    if (!fs.existsSync('config.server.json')) {
        console.log('Server config created from default.');
        fs.copySync('config.server.default.json', 'config.server.json');
    }

    config = objUtils.loadJson('config.server.json');
    if (config.componentPath !== undefined) {
        app.componentPath = config.componentPath;
    }
    if (config.subCircuitPath !== undefined) {
        app.subCircuitPath = config.subCircuitPath;
    }
    if (config.elasticSearchURI !== undefined) {
        app.elasticSearchURI = config.elasticSearchURI;
    }
    if (config.port !== undefined) {
        app.port = config.port;
    }
    if (config.autoRescan !== undefined) {
        exports.autoRescan = config.autoRescan;
    } else {
        exports.autoRescan = true;
    }
};

(function () {
    var useNext = false;
    var i = 0;
    gitScanner.disableCache = true;
    subCircuitScanner.disableCache = true;
    parseConfig();
    process.argv.forEach(function (val, index, array) {
        i++;
        if (useNext) {
            app.componentPath = val;
            useNext = false;
        } else if (val === '-test') {
            gitScanner.disableCache = true;
            subCircuitScanner.disableCache = true;
            app.componentPath = 'mocha-tests/test-components';
            app.subCircuitPath = 'mocha-tests/subcircuits';
            exports.autoRescan = false;
        } else if (val === '-usecache') {
            gitScanner.disableCache = false;
            subCircuitScanner.disableCache = false;
        } else if (val === '-cp') {
            useNext = true;
            app.componentPath = 'unknown';
        } else if (i > 2) {
            console.error('Unknown command line argument: ' + val);
            process.exit(-1);
        }
         //   console.log(index + ': ' + val);
    });
    app.componentPath = path.resolve(app.componentPath);
    if (!fs.existsSync(app.componentPath)) {
        console.error('Component path ' + app.componentPath + ' does not exist.  Cannot continue.');
        process.exit(-1);
    }
    if (app.subCircuitPath !== undefined  &&  app.subCircuitPath !== '') {
        app.subCircuitPath = path.resolve(app.subCircuitPath);
        app.use('/component_files', express.static(app.componentPath));
    }
})();

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send(err + ' ' + err.message);
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send('error');
});

console.log('Component path = ' + app.componentPath);
console.log('Sub-circuit path = ' + app.subCircuitPath);

esUtils.connect(app.elasticSearchURI);

gitScanner.initialize(app.componentPath, function () {
});
if (app.subCircuitPath !== undefined && app.subCircuitPath !== '') {
    subCircuitScanner.initialize(app.subCircuitPath);
}

module.exports = app;
