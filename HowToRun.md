Prerequisites:

* Elasticsearch must be configured and running.  (v1.4.2 is known to work)

Steps:

* `bower install`. If you don't have bower, `npm install -g bower`

* `npm install`

* Copy `config.server.default.json` to `config.server.json`, and edit to configure the backend

* Copy `client_apps/componentBrowser/config.client.default.json` to `client_apps/componentBrowser/config.client.json` and edit to reflect the backend setup

* `gulp compile-all`. If you don't have gulp, `npm install -g gulp`

* `node bin/www`

* Visit `http://localhost:3000/componentBrowser/index.html`
