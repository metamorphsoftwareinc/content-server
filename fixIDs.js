/**
 * This stand-alone program was used to spin through files in the component git repository and
 * make changes to each .acm file.
 *
 * @type {exports}
 */


'use strict';


var fs = require('fs');
var path = require('path');
var xml2js = require('xml2js');
var uuid = require('node-uuid');

var badIDs = {};

badIDs['27b93319-feb5-45b7-b358-0b009ef7088f'] = undefined;
badIDs['3655b29b-c1fe-461b-8f16-b249a1d603d1'] = undefined;
badIDs['44429c57-327c-498b-aea9-907e17744256'] = undefined;
badIDs['f50a4854-bc10-4b34-87b2-8137fa70243a'] = undefined;
badIDs['b2550316-ab9d-4b76-aa9c-eccfb0503003'] = undefined;
badIDs['a18264ce-087a-4e12-b551-82fad90908c7'] = undefined;
badIDs['350815db-9730-4514-8dd0-ade91b09fe04'] = undefined;
badIDs['d5b638ca-659b-4f79-b54b-dc4e9aeb0cc4'] = undefined;
badIDs['8196a1ea-ccd5-439e-bb8f-921987e69ffd'] = undefined;
badIDs['429677af-b65c-4d82-963f-482eedf4e30e'] = undefined;
badIDs['1c875518-5f31-4c3d-a2f4-9ce1a279b04d'] = undefined;
badIDs['b94a179f-d0a2-4c11-8f9c-9781573ca6d3'] = undefined;
badIDs['b99b113d-70b3-416d-ab65-54cf4167dcb7'] = undefined;
badIDs['0ad1737f-88d9-4126-9b6c-57ae0b4bd69c'] = undefined;
badIDs['1947a37c-9979-4fd7-b59e-fd40d64c2f47'] = undefined;
badIDs['f1fcab7a-1946-44e1-9007-2937057f3084'] = undefined;
badIDs['19aad224-21cd-4c36-8115-4494e2649a08'] = undefined;
badIDs['a35af4f3-82e9-4856-b607-af37eccd9da9'] = undefined;
badIDs['7b3d37e1-aeb9-4f0c-888e-1854f372e118'] = undefined;
badIDs['791b4bf3-6533-48bd-ad74-97fef040c294'] = undefined;
badIDs['8d2fb355-1629-4847-b3cb-f73160b35198'] = undefined;
badIDs['4fb19bbd-a8af-4385-b566-b564d252b6f1'] = undefined;
badIDs['b9996bcc-98dc-494f-81b3-30da7f0941c7'] = undefined;
badIDs['f53cea4d-86d3-4b65-987d-ef99cc1a476f'] = undefined;
badIDs['b29812e1-7556-49e4-8555-742b2e1f1e89'] = undefined;
badIDs['8af2af74-2301-4fba-8926-61d490a5bcea'] = undefined;
badIDs['97c94e73-4f24-4186-b8fe-f3a7d01fa61b'] = undefined;
badIDs['c952c391-5d30-4845-960e-6c37de2890e6'] = undefined;
badIDs['d381a252-597c-414d-a75e-c724771fe662'] = undefined;


var badCount = 0;
var totalFiles = 0;

var readXml = function (filename)
{
    var res;
    (new xml2js.Parser()).parseString(fs.readFileSync(filename), function (err, result) {
        res = result;
    });
    return res;
};

var fixID = function (filename) {
    totalFiles++;
    var acm = readXml(filename);
    if (acm.Component.$.ID in badIDs) {
        badCount++;

        var file = fs.readFileSync(filename).toString();
        file = file.split('\n');
        var replaced = false;
        for (var i=0 ; i < file.length ; i++) {
            var line = file[i];
            if (line.indexOf('<Component xmlns') === 0) {
                file[i] = line.replace(/ ID='[^']*'/, ' ID=\'' + uuid.v4() + '\'');
                replaced = true;
                break;
            }
        }
        if (replaced) {
            fs.writeFileSync(filename, file.join('\n'));
        }
    }
};

var readDir = function (start)
{
    var files = fs.readdirSync(start);
    for (var i=0 ; i < files.length ; i++) {
        var fileName = files[i];
        var fullPath = path.join(start, fileName);
        var stat = fs.statSync(fullPath);
        if (stat.isFile()) {
            if (path.extname(fileName) === '.acm') {
                fixID(fullPath);
            }
        } else {
            readDir(fullPath);
        }
    }
};

readDir('/home/blake/tonkalib.git/components');
console.log(badCount);
console.log(totalFiles);
