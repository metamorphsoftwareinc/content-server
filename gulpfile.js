'use strict';

var
    argv = require('yargs').argv,
    debug = !argv['production'],
    debugShim = false, //this is for toggling browserify shim debug

    bowerDistributeFolder = './dist',

    builtFilesToCopyOverToDist = [
        'public/componentBrowser/scripts/*.js',
        'public/componentBrowser/styles/*.css',
        'public/subcircuitBrowser/scripts/*.js',
        'public/subcircuitBrowser/styles/*.css'
    ],

    buildRoot = './public/',

    clientSourcesFolders = './client_apps/',

    gulp = require('gulp'),
    eslint = require('gulp-eslint'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    buffer = require('gulp-buffer'),
    sass = require('gulp-sass'),
    runSequence = require('run-sequence'),
    sourcemaps = require('gulp-sourcemaps'),
    clean = require('gulp-clean'),
    templateCache = require('gulp-angular-templatecache'),
    prettify = require('gulp-js-prettify'),
    path = require('path'),
    template = require('gulp-template'),
    babelify = require("babelify"),
    fs = require('fs-extra'),
    gulpNgConfig = require('gulp-ng-config'),
    derequire = require('gulp-derequire');

require('path');


function swallowError(error) {

    //If you want details of the error in the console
    console.log(error.toString());

    this.emit('end');
}


gulp.task('clean-build', function() {
    return gulp.src(buildRoot).pipe(clean());
});

gulp.task('clean-dist', function() {
    return gulp.src(bowerDistributeFolder).pipe(clean());
});

// Application tasks
var applications = [
    'componentBrowser',
    'subcircuitBrowser'
    ],
    i,
    registerAppTasks,
    gulpAppTaskNames = [],
    imagePatterns;

imagePatterns = [
    '**/*.png',
    '**/*.jpg',
    '**/*.svg'
];


registerAppTasks = function(appName) {

    var appSourceRoot = clientSourcesFolders + appName + '/',
        appBuildRoot = buildRoot + appName + '/',

        appSources = [appSourceRoot + '**/*.js'],
        appModuleScript = appSourceRoot + 'app.js',

        appIndexFile = appSourceRoot + 'index.html',
        appTemplates = [appSourceRoot + '*/**/*.html'],

        appLibs = require(appSourceRoot + '/libs.json'),

        appTemplateModule = 'mms.' + appName + '.templates',

        appStyles = [appSourceRoot + '/**/*.scss'],

        appImages = imagePatterns.map(function(imageType) {
            return appSourceRoot + imageType;
        });

    if (!fs.existsSync(appSourceRoot + 'config.client.json')) {
        console.log('Client config created from default.');
        fs.copySync(appSourceRoot + 'config.client.default.json', appSourceRoot + 'config.client.json');
    }

    gulp.src(appSourceRoot + 'config.client.json')
        .pipe(gulpNgConfig('mms.' + appName + '.config', {
            wrap: '/*globals angular*/\n/*eslint-disable */\n<%= module %>'
        }))
        .pipe(rename(function(path) {
            path.basename = 'appConfig';
            path.extname = '.js';
        }))
        .pipe(gulp.dest(appSourceRoot));

    gulp.task('lint-' + appName, function() {

        console.log('Linting ' + appName + '-app...');

        gulp.src(appSources)
            .pipe(eslint())
            .pipe(eslint.format());
    });

    gulp.task('browserify-' + appName, function() {

        var bundler, bundle;
        console.log('Browserifying ' + appName + '-app...');

        if (debugShim) {
            process.env.BROWSERIFYSHIM_DIAGNOSTICS = 1;
        }
        bundler = browserify({
            entries: [appModuleScript],
            debug: true
        });

        bundle = function() {
            return bundler
                .transform(babelify)
                .bundle()
                .on('error', swallowError)
                .pipe(source(appSourceRoot + 'app.js'))
                .pipe(buffer())
                .pipe(sourcemaps.init({
                    loadMaps: true
                }))
                .pipe(rename(function(path) {
                    path.dirname = '';
                    path.basename = appName;
                    path.extname = '.js';
                }))
                // Add transformation tasks to the pipeline here.
                //.pipe(uglify())
                .pipe(sourcemaps.write('./'))
                .pipe(gulp.dest(appBuildRoot + 'scripts/'));
        };

        return bundle();
    });

    gulp.task('copy-' + appName + '-libs', function() {

        var styleMapFileNames,
            scriptMapFileNames,
            scriptMapMapFileNames;

        console.log('Copying ' + appName + '-libs...');

        styleMapFileNames = appLibs.styles.map(function(fileName) {
            return path.join(
                path.dirname(fileName),
                path.basename(fileName, '.css') + '.map'
            );
        });

        scriptMapFileNames = appLibs.scripts.map(function(fileName) {
            return path.join(
                path.dirname(fileName),
                path.basename(fileName, '.js') + '.map'
            );
        });

        scriptMapMapFileNames = appLibs.scripts.map(function(fileName) {
            return path.join(
                path.dirname(fileName),
                path.basename(fileName) + '.map'
            );
        });

        gulp.src(appLibs.styles)
            .pipe(rename(function(path) {
                path.dirname = 'libs';
            }))
            .pipe(gulp.dest(appBuildRoot));

        gulp.src(appLibs.scripts)
            .pipe(rename(function(path) {
                path.dirname = 'libs';
            }))
            .pipe(gulp.dest(appBuildRoot));

        gulp.src(appLibs.libs)
            .pipe(rename(function(path) {
                path.dirname = 'libs';
            }))
            .pipe(gulp.dest(appBuildRoot));

        gulp.src(styleMapFileNames)
            .pipe(rename(function(path) {
                path.dirname = 'libs';
            }))
            .pipe(gulp.dest(appBuildRoot));

        gulp.src(scriptMapFileNames)
            .pipe(rename(function(path) {
                path.dirname = 'libs';
            }))
            .pipe(gulp.dest(appBuildRoot));

        gulp.src(scriptMapMapFileNames)
            .pipe(rename(function(path) {
                path.dirname = 'libs';
            }))
            .pipe(gulp.dest(appBuildRoot));

        gulp.src(appLibs.fonts)
            .pipe(rename(function(path) {
                path.dirname = 'fonts';
            }))
            .pipe(gulp.dest(appBuildRoot));

    });

    gulp.task('compile-' + appName + '-templates', function() {

        var scriptFileNames,
            styleFileNames;

        console.log('Compiling ' + appName + '-app-templates...');

        gulp.src(appTemplates)
            .pipe(rename(function(path) {
                path.dirname = 'templates';
            }))
            .pipe(templateCache(appName + '-templates.js', {
                module: appTemplateModule,
                standalone: true,
                root: '/' + appName + '/'
            }))
            .pipe(gulp.dest(appBuildRoot + '/scripts/'));

        scriptFileNames = appLibs.scripts.map(function(fileName) {
            return 'libs/' + path.basename(fileName);
        });


        styleFileNames = appLibs.styles.map(function(fileName) {
            return 'libs/' + path.basename(fileName);
        });

        gulp.src(appIndexFile)
            .pipe(template({
                scripts: scriptFileNames,
                styles: styleFileNames
            }))
            .pipe(gulp.dest(appBuildRoot));
    });

    gulp.task('compile-' + appName + '-images', function() {

        console.log('Compiling images...');

        gulp.src(appImages)
            .pipe(rename(function(path) {
                path.dirname = '';
            }))
            .pipe(gulp.dest(appBuildRoot + 'images/'));
    });


    gulp.task('compile-' + appName + '-styles', function() {

        console.log('Compiling ' + appName + '-app styles...');

        gulp.src(appStyles)
            // The onerror handler prevents Gulp from crashing when you make a mistake in your SASS
            .pipe(sass({
                errLogToConsole: true,
                sourceComments: 'map'
            }))
            .pipe(rename(function(path) {
                path.dirname = '';
            }))
            .pipe(concat(appName + '-app.css'))
            .pipe(gulp.dest(appBuildRoot + 'styles/'));
    });

    gulp.task('compile-' + appName + '-scripts', ['lint-' + appName,
            'browserify-' + appName
        ],
        function() {
            console.log('Compiling ' + appName + ' scripts');
        }
    );

    gulp.task('compile-' + appName, [
            'compile-' + appName + '-scripts',
            'compile-' + appName + '-templates',
            'compile-' + appName + '-styles',
            'compile-' + appName + '-images'
        ],
        function() {
            runSequence('copy-' + appName + '-libs');
            console.log('Compiling ' + appName);
        });

    gulpAppTaskNames.push('compile-' + appName);

};

for (i = 0; i < applications.length; i += 1) {
    registerAppTasks(applications[i]);
}

gulp.task('refresh-server', function() {
    //
    //console.log('Refreshing server...');
    //
    //refresh(lrserver);
});



gulp.task('compile-dist', ['clean-dist'],
    function() {

        console.log('Compiling Bower dist package');

        gulp.src(builtFilesToCopyOverToDist)
        .pipe(rename(function (path) {
                    path.dirname = '';
        }))
        .pipe(gulp.dest(bowerDistributeFolder));

    }
);


gulp.task('register-watchers', ['compile-all'], function(cb) {
    var i,
        registerAppWatchers;

    registerAppWatchers = function(appName) {

        var appSourceRoot = clientSourcesFolders + appName + '/',

            appSources = [
                clientSourcesFolders + '*.js',
                clientSourcesFolders + '**/*.js',
                clientSourcesFolders + '*.jsx',
                clientSourcesFolders + '**/*.jsx'

                ],

            appHtmls = [appSourceRoot + '**/*.html'],

            appLibs = appSourceRoot + 'libs.json',

            appStyles = [appSourceRoot + '/**/*.scss'],

            appImages = imagePatterns.map(function(imageType) {
                return appSourceRoot + imageType;
            });

        gulp.watch(appSources, ['compile-' + appName + '-scripts', 'refresh-server']);
        gulp.watch(appHtmls, ['compile-' + appName + '-templates', 'refresh-server']);
        gulp.watch(appStyles, ['compile-' + appName + '-styles', 'refresh-server']);
        gulp.watch(appImages, ['compile-' + appName + '-images', 'refresh-server']);
        gulp.watch(appLibs, ['compile-' + appName, 'refresh-server']);

    };

    for (i = 0; i < applications.length; i += 1) {
        registerAppWatchers(applications[i]);
    }

    gulp.watch('src/plugins/**', ['rjs-build']);
    return cb;
});


gulp.task('compile-all', [  ], function(cb) {
    runSequence('clean-build', gulpAppTaskNames, 'compile-dist', cb);
});
